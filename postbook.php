<?php
	require_once 'vendor/autoload.php';
	use App\Templates\IndexTemplate;
	use App\Templates\PostBookTemplate;
	use App\Middleware\PostBook;	

	$indexTemplate = new IndexTemplate();
	$postBookTemplate = new PostBookTemplate();
	$postBook = new PostBook();

	if(isset($_POST['submit'])){
			
		$postBook->beginSession();
		$postBook->authenticateSession();
		$postBookTemplate->result = $postBook->utilizeSession();
	}
	$indexTemplate->page = "post book";
	$indexTemplate->header();
	$postBookTemplate->displayPageContent();
	$indexTemplate->footer();
?>