<?php
	require_once 'vendor/autoload.php';

	use App\Middleware\EditBook;
	use App\Classes\LinkManager;
	use App\Classes\InputValidator;	
	use App\Templates\EditBookTemplate;	

	$result = "";
	$editBook = new EditBook();	
	$linkManager = new LinkManager();
	$inputValidator = new InputValidator();
	$editBookTemplate = new EditBookTemplate();

	$editBook->beginSession();
	$editBook->authenticateSession();

	if(isset($_GET['item'])){
		$editBook->bookId = $linkManager->decodeUrlId(
							$inputValidator->validateInput($_GET['item'])
							);		
	}

	$result = $editBook->utilizeSession();

	if(isset($_POST['submit'])){		

		$editBook->validateData();
		$message = $editBook->utilizeData();		
	}
?>
<html>
	<head>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>


		<style>
			body{
				padding:0px;
				margin:0px;	
				background:#eaebed;			
			}
			body,header,.content,footer{
				display:block;
			}
			.error{
				color:#000;
				background:#ffd1d1;
				border:#e03838 solid 0.1em;
				border-radius:2px;
				padding:1%;
				width:90%;
				margin-left:5%;
				text-align:center;
				margin-bottom:10px;
			}
			.success{
				color:#000;
				background:#e1ffe0;
				border:#53c14f solid 0.1em;
				border-radius:2px;
				padding:1%;
				width:80%;
				margin-left:10%;
				text-align:center;
				margin-bottom:10px;
			}
			.success span{
				font-weight:600;
			}
			.book-post-success-btn{
				color:#000;
				background:#53c14f;
				border:#53c14f solid 0.1em;
				border-radius:5px;
				text-align:center;
				padding:1%;
				width:25%;
				margin-left:15%;
				float:left;
			}
			.book-post-success-btn a{
				color:#fff;
				text-decoration:none;
			}
			header{
				width:100%;				
				border-bottom:#ccc solid 0.1em;				
				padding:1%;
				padding-top:0.5%;
				background:#fcfcfc;
				margin-top:0px;
			}
			.clear{
				clear:both;
			}
			.logo{
				width:17.5%;
				margin-left:2.5%;
				float:left;
				/*border:#000 solid 0.1em;*/
			}
			.logo img{
				width:80%;
				margin-left:10%;
			}
			.menu{
				width:60%;
				margin-left:36%;
				/*border:#000 solid 0.1em;*/
			}
			.menu ul{
				padding:0px;
				margin:0px;
				list-style:none;
				margin-top:3%;
				margin-left:5%;
			}
			.menu li{
				float:left;
				margin-left:2.5%;
				/*border:#000 solid 0.1em;*/
				font-size:15px;
			}
			.menu li:nth-child(2),.menu li:nth-child(3),.menu li:nth-child(4),.menu li:nth-child(5){				
				margin-top:5px;
			}
			.menu li a{
				color:#515050;
			}
			.menu .how-it-works-btn{
				border:#ccc solid 0.1em;
				text-align:center;
				padding:4%;
				border-radius:5px;
				width:110px;
				margin-left:-20%;
			}
			.menu .how-it-works-btn a{
				color:#565555;
				text-decoration:none;
				font-size:15px;
			}
			.menu li img{
				width:30px;
				margin-top:2px;
			}
			li .post-book-btn{
				border:#fff solid 0.1em;
				text-align:center;
				padding:4%;
				background:#3d76d3;
				border-radius:5px;
				width:110px;
				margin-left:20%;			
			}
			li .post-book-btn a{
				text-decoration:none;
				color:#fff;
				font-size:15px;
			}
			.content{
				padding:1%;
				background:#eaebed;
				width:90%;
				margin-left:4.5%;
			}
			.post-book{
				width:100%;
				background:#fcfcfc;
				padding:2%;
				font-size:14px;
			}
			.post-book h3{
				text-align:center;
			}
			.post-book h4{
				font-size:18px;
				color:#6f706f;
				text-align:center;
				margin-bottom:20px;
				border-bottom:#ccc solid 0.1em;
				background:#e0e0e0;
				padding:1%;
			}
			.post-book form{
				padding:1%;
				/*border:#ccc solid 0.1em;*/
			}
			.post-book form div{
				margin-top:5px;
			}
			.post-book form input[type="text"]{
				width:100%;
				border-radius:2px;
				border:#ccc solid 0.1em;
				padding:1%;
				background:#f2f2f2;
				font-weight:600;
			}		
			.post-book form input[type="file"]{
				width:100%;
				border-radius:2px;
				border:#ccc solid 0.1em;
				padding:1%;
				background:#f2f2f2;
			}
			.post-book form select{
				width:100%;
				border-radius:2px;
				border:#ccc solid 0.1em;
				padding:1%;
				background:#f2f2f2;
				font-weight:600;
			}
			.post-book form textarea{
				width:100%;
				border-radius:2px;
				border:#ccc solid 0.1em;
				padding:1%;
				background:#f2f2f2;
				font-weight:600;
			}
			.post-book form input[type="submit"]{
				width:20%;
				margin-left:77%;
				margin-top:10px;
				border-radius:2px;
				background:#39cc42;
				color:#fff;
				cursor:pointer;
				border:#fcfcfc solid 0.1em;
				border-radius:5px;
				padding:1%;
				text-align:center;
			}
			.book-info,.book-residence,.book-status{
				float:left;
				width:30%;
				margin-left:2.5%;
				border:#ccc solid 0.1em;
				padding:1%;
				height:450px;
				overflow:auto;
			}
			.book-info-img{
				width:50%;
				margin-left:25%;
				border:#ccc solid 0.1em;
				height:140px;
				overflow:hidden;
			}
			.book-info-img img{
				width:100%;
			}
			.post-book .book-status div:last-child{
				width:100%;
				border-radius:2px;
				border:#ccc solid 0.1em;
				padding:1%;
			}

			footer{
				width:50%;
				margin-left:33%;
				padding:1%;
			}
			footer ul{
				list-style:none;
				margin:0px;
				padding:0px;
			}
			footer li{
				float:left;
				margin-left:10px;
			}
			footer li a{
				color:#3d76d3;
				text-decoration:none;
			}
			footer li a img{
				width:20px;
				height:20px;
			}
			

		</style>
	</head>
	<body>
		<header>
			<div class="logo">
				<a href="home.php"><img src="logo.png"/></a>
			</div>
			<div class="menu">
				<ul>
					<!-- <li>
						<select>
							<option>Select country</option>
						</select>						
					</li> -->					
					<li>
						<div class="how-it-works-btn"><a href="">How it works</a></div>
					</li>
					<li><a href="home.php">home</a></li>
					<li><a href="mybooks.php">my books</a></li>
					<li><a href="profile.php">my profile</a></li>
					<li><a href="index.php">logout</a></li>
					<li><a href=""><img src="notify.png"/></a></li>
					<li><a href=""><img src="twitter-circle.png"/></a></li>
					<li><a href=""><img src="fb-circle.png"/></a></li>
					<li>
						<div class="post-book-btn"><a href="postbook.php">Post a book</a></div>
					</li>
					<div class="clear"></div>	
				</ul>
			</div>
			<div class="clear"></div>
		</header>

		<div class="content">		
		<?php
			if(!empty($message[0])){
				echo $message[1];
			}else{
				$editBookTemplate->displayEditBookForm($result,$editBook->bookId);
			}
		?>
		</div>
		<footer>
			<p>
				<ul>
					<li>&copy; 2018 bukswap</li>
					<li><a href="">About</a></li>
					<li><a href="">Privacy and policy</a></li>
					<li><a href=""><img src="twitter.png"/></a></li>
					<li><a href=""><img src="fb.png"/></a></li>
					<div class="clear"></div>
				</ul>
			</p>
		</footer>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		<?php
			if($message[0] == "success"){
				echo "$('form').css('display','none');";
			}
		?>		
	});
</script>