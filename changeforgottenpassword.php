<?php
require_once 'vendor/autoload.php';

use App\Middleware\ChangeForgottenPassword;
use App\Templates\ChangeForgottenPasswordTemplate;
use App\Templates\IndexTemplate;

$changeForgottenPassword = new ChangeForgottenPassword();
$changeForgottenPasswordTemplate = new ChangeForgottenPasswordTemplate();
$indexTemplate = new IndexTemplate();

if(isset($_GET['id'])){
	$changeForgottenPasswordTemplate->id = $changeForgottenPassword->getSellerIdFromUrl();
}

if(isset($_POST['submit'])){
	$changeForgottenPassword->captureData();
	$error = $changeForgottenPassword->validateData();

	if(empty($error)){

		if($changeForgottenPassword->utilizeData()){
			$changeForgottenPasswordTemplate->message = "<div class='success'>Your password was changed successfully. Proceed to accounts to login.</div>";
		}else{
			$changeForgottenPasswordTemplate->message = "<div class='error'>This process failed. Try again later!</div>";
		}
	}else{
		$changeForgottenPasswordTemplate->message = "<div class='error'>".$error."</div>";
	}
}

$indexTemplate->page = "change forgotten password";
$indexTemplate->header();
$changeForgottenPasswordTemplate->displayPageContent();
$indexTemplate->footer();