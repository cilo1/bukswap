<?php
	require_once 'vendor/autoload.php';

	use App\Middlewares\SearchBook;
	use App\Templates\IndexTemplate;
	use App\Templates\HomeTemplate;

	$searchBook = new SearchBook();

	$searchBook->beginSession();
	$searchBook->authenticateSession();

	$indexTemplate = new IndexTemplate();
	$homeTemplate = new HomeTemplate();	

	if(isset($_POST['search'])){		

		$homeTemplate->result = $searchBook->utilizeSession();
		$homeTemplate->searchItem = $_POST['item'];
	}
	$indexTemplate->page = "home";
	$indexTemplate->header();
	$homeTemplate->displayPageContent();
	$indexTemplate->footer();
?>