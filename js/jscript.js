$(document).ready(function () {
	// Show the div after 5s    
    setTimeout(function() {
		$(".success").css("display","none").fadeOut(1000);
	},5000);

	setInterval(function(){
		$.ajax({
			method: "POST",
			url: 'processnotification.php',
			data: {action:'notification-pop'},
			success: function(data){
				//alert(data);
				if(data > 0){
					$('#notification-menu-pop').css("display","block");
					if(data > 10){
						data = "10+";
						$('#notification-menu-pop').text(data);
					}else{
						$('#notification-menu-pop').text(data);
					}
				}else{
					$('#notification-menu-pop').css("display","none");
				}
			}
		});
	},100);

	$('.menu-mobile').click(function(){
		$('.menu-mobile-content').css("display","block");
		$('.content').css("display","none");
	});

	$('.mobile-account-form-switch').click(function(){
		var content = $(this).text();
		if(content == "Login to account"){
			$('.signup').css("display","none");
			$('.login').css("display","block");
			$(this).text("Create new account");
		}else{
			$('.signup').css("display","block");
			$('.login').css("display","none");
			$(this).text("Login to account");
		}
	});

	$('.menu-mobile-content-close-btn').click(function(){
		$('.menu-mobile-content').css("display","none");
		$('.content').css("display","block");
	});

	$('.account-link').click(function(e){
		e.preventDefault();
		$('.account-content-pop').css("display","block");
		$('a .account-link').removeClass('account-link').add('account-unlink');
	});

	$('.account-unlink').click(function(e){
		e.preventDefault();
		$('.account-content-pop').css("display","none");
		$('a .account-unlink').removeClass('account-unlink').add('account-link');
	});

	$('.notification-link').click(function(e){
		e.preventDefault();
		$('.notification-content-pop').css("display","block");							
		$.ajax({
			method: "POST",
			url: 'processnotification.php',
			data: {action:'notification-data'},
			success: function(data){
				//alert(data);
				if(data == 0){
					$('.notification-content-info').
					html("<div class='notification-content-info-item'>"+
					"<p>No messages yet! </p></div>");
				}else{
					var obj = JSON.parse(data);
					var fragment = document.createDocumentFragment();					

					if(obj.notifications.length > 0){
						for(var i = 0;i < obj.notifications.length; i++){
							//console.log(i);
							// $('.notification-content-info').
							// html("<div class='notification-content-info-item'>"+
							// 	"<div class='notification-content-info-item-date'>"+
							// 	obj.notifications[i].date_created+
							// 	"</div>"+obj.notifications[i].notification_message+
							// 	"</div>");	
							var content_el = document.createElement("div");
							var subcontent_el = document.createElement("div");

							content_el.className = "notification-content-info-item";
							subcontent_el.className = "notification-content-info-item-date";
							content_el.innerText = obj.notifications[i].notification_message;
							subcontent_el.innerText = obj.notifications[i].date_created;

							content_el.appendChild(subcontent_el);
							fragment.appendChild(content_el);				
						}
						$('.notification-content-info').append(fragment);
						document.getElementByClass('.notification-content-info').style.display = "block";	
					}
				}                
			}
		});	
	});

	$('.notification-content-header .close-btn').click(function(){
		$('.notification-content-pop').css("display","none");
	});

	$('.mybooks-item .delete-link').click(function(e){
		e.preventDefault();
		var cur = $('.mybooks-item .delete-link').index($(this)); // get the index of the clicked button within the collection
     	var id = $('.mybooks-item .delete-link').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
		
		var title = $('.mybooks-item-info h5').eq(cur).text();
		$('.confirmation-box').css("display","block");
		$('.books').css("display","none");
		$('.confirmation-box h5').text(title);
		$('.confirmation-box p span').text("my books");
		$('.confirmation-box .yes a').attr('href',id);
	});

	$('.confirmation-box .cancel').click(function(){
		$('.confirmation-box').css("display","none");
		$('.books').css("display","block");
	});

	$('.confirmation-deactivation .cancel').click(function(){
		$('.confirmation-deactivation').css("display","none");
		$('.profile').css("display","block");
	});

	$('.confirmation-box .yes a').click(function(e){
		e.preventDefault();		
		var item = $('.confirmation-box span').text();

		if(item == "my books"){
			var id = $(this).attr('href');
			$.ajax({
				method: "POST",
				url: 'processbook.php',
				data: {id: id, action: "delete_mybooks"},
				success: function(data){
					//alert(data);
					if(data == 1){
						location.reload(true);
					}
				}
			});
		}
		if(item == "watchlist"){
			var item = $('.confirmation-box h5').text();
			$.ajax({
				method: "POST",
				url: 'processwatchlist.php',
				data: {title: item, action: "remove"},
				success: function(data){
					if(data == 1){
						location.reload(true);
					}
				}
			});
		}
		if(item == "booklist"){
			var id = $(this).attr('href');			
			$.ajax({
				method: "POST",
				url: 'processbooklist.php',
				data: {item: id, action: "remove"},
				success: function(data){
					//alert(data);
					if(data == 1){
						location.reload(true);
					}
				}
			});
		}		
		$('.confirmation-box').css("display","none");
		$('.books').css("display","block");
	});

	$('.mywatchlist-show-more-btn').click(function(){
		var show = $(this).text();

		if(show == "show less"){

			$('.mywatchlist').css({
				"max-height":"250px",
	 			"overflow":"hidden"
			}).fadeIn(1000);
			$(this).text("show more");

		}else{

			$('.mywatchlist').css({
				"max-height":"450px",
	 			"overflow":"auto"
			}).fadeIn(1000);
			$(this).text("show less");
		}		
	});

	$('.mybooklist-show-more-btn').click(function(){
		var show = $(this).text();

		if(show == "show less"){

			$('.mybooklist').css({
				"max-height":"250px",
	 			"overflow":"hidden"
			}).fadeIn(1000);
			$(this).text("show more");

		}else{

			$('.mybooklist').css({
				"max-height":"450px",
	 			"overflow":"auto"
			}).fadeIn(1000);
			$(this).text("show less");
		}		
	});

	$('.mywatchlist-item .remove-watchlist-btn').click(function(e){
		e.preventDefault();
		var cur = $('.mywatchlist-item .remove-watchlist-btn').index($(this)); // get the index of the clicked button within the collection
     	var title = $('.mywatchlist-item-info a').eq(cur).text(); // find the input with the contentid class at the same index and get its value
		
		$('.confirmation-box').css("display","block");
		$('.books').css("display","none");
		$('.confirmation-box h5').text(title);
		$('.confirmation-box span').text("watchlist");
		//$('.confirmation-box .yes a').attr('href',id);		
	});

	$('.mybooklist-item .remove-booklist-btn').click(function(e){
		e.preventDefault();
		var cur = $('.mybooklist-item .remove-booklist-btn').index($(this)); // get the index of the clicked button within the collection
     	var id = $('.mybooklist-item .remove-booklist-btn').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
		var title = $('.mybooklist-item-info a').eq(cur).text();

		$('.confirmation-box').css("display","block");
		$('.books').css("display","none");
		$('.confirmation-box h5').text(title);
		$('.confirmation-box span').text("booklist");
		$('.confirmation-box .yes a').attr('href',id);		
	});

	$('.did-you-find-btn a').click(function(e){
		e.preventDefault();
		var cur = $('.did-you-find-btn a').index($(this)); // get the index of the clicked button within the collection
     	var id = $('.did-you-find-link').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
			
		$('.did-you-find-response').eq(cur).css('display','block');
	});

	$('.did-you-find-response .response-link').click(function(e){
		e.preventDefault();
		var cur = $('.did-you-find-response .response-link').index($(this)); // get the index of the clicked button within the collection
		//$('.did-you-find-response').eq(cur).css('display','none');
		var response = $('.response-link').eq(cur).text();
		var id = $('.response-link').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
		
		$.ajax({
			method: "POST",
		    url: 'processbookdelisting.php',
		    data: {response: response,id: id},
		    beforeSend: function() {
		    	if(response != "done"){		    		
		       		$('.response-link').eq(cur).text('sending...');
		    	}
		    },
		    success: function(data){
			    //alert(data);
			    if(data == 1){
			       	$('.did-you-find-response').css("display","none").fadeOut(1000);
			       	$('.notify').text("Feedback was successfully sent, thank you!");
			       	$('.notify').css("display","block").fadeIn(2000);
			       	 $('.response-link').eq(cur).text('done');  	         				
			    }
		    },
		    error: function(a,b,c) {
		        // some debug could be here
		    }
		});
		setTimeout(function() {
			$('.notify').css("display","none").fadeOut(1000);
		},7000); 
	});

	$('.did-you-find-response .close-link').click(function(e){
		e.preventDefault();
		var cur = $('.did-you-find-response .close-link').index($(this)); // get the index of the clicked button within the collection
		$('.did-you-find-response').eq(cur).css('display','none');
	});

	$('remove-watchlist-btn').click(function(){
		$('.confirmation-box').css('display','block');
		$('.mybooks').css('display','none');
	});

	$('.add-to-watchlist-btn a').click(function(e){
			e.preventDefault();

			var item = $('#searchedItem').text();
			var encodedItem = $(this).attr('href');

			$.ajax({
			    method: "POST",
			    url: 'processwatchlist.php',
			    data: {title: item,action: "add"},
			    beforeSend: function(){
			    	if (sessionStorage.getItem('status') != 'loggedIn'){
					    //redirect to page
			    		window.location.replace('account_credentials.php?n=watchlist&&id='+encodedItem);
					}
			    },
			    success: function(data){
			        //alert(data);       
			        if(data == "auth failed"){
			    		$('.must-be-loggedin-popup').css("display","block");
			    		$('.dark-popup-background').css("display","block");
			    		$('.must-be-loggedin-popup .proceed-btn-green a').attr('href','account_credentials.php?n=watchlist&&id='+encodedItem);
			        }

			        if(data == 1){
			        	$('.add-to-watchlist-btn').css("display","none").fadeOut(1000);
			        	$('.remove-from-watchlist-btn').css("display","block").fadeIn(1000);
			        	$('.notify').text(item+" was successfully added to your watchlist!");
			        	$('.notify').css("display","block").fadeIn(2000);  	         				
			        }
			    },
			    error: function(a,b,c) {
			        // some debug could be here
			    }
			});
			setTimeout(function() {
				$('.notify').css("display","none").fadeOut(1000);
			},5000);
		});

		$('.must-be-loggedin-popup .proceed-btn-dark a').click(function(e){
			e.preventDefault();
			$('.must-be-loggedin-popup').css("display","none");
			$('.dark-popup-background').css("display","none");
		});

		$('.must-be-loggedin-popup .close-btn').click(function(){
			$('.must-be-loggedin-popup').css("display","none");
			$('.dark-popup-background').css("display","none");
		});

		$('.must-be-loggedin-popup .proceed-btn-green').click(function(){
			
		});


		$('.remove-from-watchlist-btn').click(function(){

			var item = $('#searchedItem').text();
			
			$.ajax({
			    method: "POST",
			    url: 'processwatchlist.php',
			    data: {title: item, action: "remove"},
			    success: function(data){
			        //alert(data);
			        if(data == 1){
			        	$('.remove-from-watchlist-btn').css("display","none").fadeOut(1000);
			        	$('.add-to-watchlist-btn').css("display","block").fadeIn(1000);
			        	$('.notify').text(item+" was successfully removed from your watchlist!");
			        	$('.notify').css("display","block").fadeIn(2000);		        	         				
			        }
			    },
			    error: function(a,b,c) {
			        // some debug could be here
			    }
			});
			setTimeout(function() {
				$('.notify').css("display","none").fadeOut(1000);
			},5000);
		});

		$('.bookmark-img a').click(function(e){
			e.preventDefault();
			//var item = $('.bookmark-link').attr('href');
			var cur = $('.bookmark-img a').index($(this)); // get the index of the clicked button within the collection
     		var id = $('.bookmark-link').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
			
			$.ajax({
			    method: "POST",
			    url: 'processbooklist.php',
			    data: {item: id, action:"add"},
			    beforeSend: function(){
			    	if (sessionStorage.getItem('status') != 'loggedIn'){
					    //redirect to page
			    		window.location.replace('account_credentials.php?n=booklist&&id='+id);
					}
			    },
			    success: function(data){
			        //alert(data);
			        if(data == "auth failed"){
			    		$('.must-be-loggedin-popup').css("display","block");
			    		$('.dark-popup-background').css("display","block");
			    		$('.must-be-loggedin-popup .proceed-btn-green a').attr('href','account_credentials.php?n=booklist&&id='+id);
			        }

			        if(data == 1){
			        	// $('.remove-from-watchlist-btn').css("display","none").fadeOut(1000);
			        	// $('.add-to-watchlist-btn').css("display","block").fadeIn(1000);			        	
			        	$('.bookmark-img a #img').eq(cur).attr('src','app_img/book-marked.png');
			        	$('.bookmark-img a p').eq(cur).text("remove from booklist!");
			        	$('.notify').text("Book was successfully added to your booklist!");
			        	$('.notify').css("display","block").fadeIn(2000);		        	         				
			        }
			    },
			    error: function(a,b,c) {
			        // some debug could be here
			    }
			});
			setTimeout(function() {
				$('.notify').css("display","none").fadeOut(1000);
			},5000);	
		});

		$('.unbookmark-img a').click(function(e){
			e.preventDefault();
			//var item = $('.bookmark-link').attr('href');
			var cur = $('.unbookmark-img a').index($(this)); // get the index of the clicked button within the collection
     		var id = $('.bookmark-link').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
			
			$.ajax({
			    method: "POST",
			    url: 'processbooklist.php',
			    data: {item: id, action:"remove"},
			    success: function(data){
			        //alert(data);
			        if(data == 1){
			        	// $('.remove-from-watchlist-btn').css("display","none").fadeOut(1000);
			        	// $('.add-to-watchlist-btn').css("display","block").fadeIn(1000);			        	
			        	$('.unbookmark-img a #img').eq(cur).attr('src','app_img/book-unmarked.png');
			        	$('.unbookmark-img a p').eq(cur).text("add to booklist!");
			        	$('.notify').text("Book was successfully removed from your booklist!");
			        	$('.notify').css("display","block").fadeIn(2000);		        	         				
			        }
			    },
			    error: function(a,b,c) {
			        // some debug could be here
			    }
			});
			setTimeout(function() {
				$('.notify').css("display","none").fadeOut(1000);
			},5000);	
		});

		$('.report-post-btn a').click(function(e){
			e.preventDefault();
			var cur = $('.report-post-btn a').index($(this)); // get the index of the clicked button within the collection
     					
			$('.report-post-form').eq(cur).css("display","block");
		});

		$('.report-post-form-header .close-btn').click(function(){
			var cur = $('.report-post-form-header .close-btn').index($(this)); // get the index of the clicked button within the collection
     					
			$('.report-post-form').eq(cur).css("display","none");
		});

		$('.send-report-btn').click(function(){
			var cur = $('.send-report-btn').index($(this)); // get the index of the clicked button within the collection
     		var id = $('.report-post-btn a').eq(cur).attr('href'); // find the input with the contentid class at the same index and get its value
			var claim = $('input[name=report-post]:checked').val();

			$.ajax({
				method:"POST",
				url:"processreportedpost.php",
				data:{id,id,claim:claim},
				beforeSend: function(){
					$('.report-post-form div').eq(cur).css("display","none");
					$('.report-post-form').eq(cur).text("sending...");
				},
				success: function(data){
					//alert(data);
					if(data == 1){
						setTimeout(function() {
							$('.report-post-form').eq(cur).text("Successfully sent!");
						},5000);
					}else{
						$('.report-post-form').eq(cur).text("Process failed, try again later!");
					}
				},
				error: function(a,b,c){

				}
			});
		});

		$('.account-deactivate-btn a').click(function(e){
			e.preventDefault();			

			$('.error').css("display","none");
			$('.confirmation-deactivation').css("display","block");
			$('.profile').css("display","none");
			
		});

		$('.confirmation-deactivation .yes a').click(function(e){
			e.preventDefault();	
			var pwd = $('input').val();
			if(pwd.length > 0){

				$.ajax({
					method: "POST",
					url: "deactivateaccount.php",
					data:{pwd: pwd},
					success: function(data){
						//alert(data);
						if(data == 0){
							$('.error').css("display","block");
							$('.error').text("Password is incorrect!");							
						}else{
							location.reload(true);
						}
					},
				    error: function(a,b,c) {
				        // some debug could be here
				    }
				});
			}else{
				$('.error').css("display","block");
				$('.error').text("Password is required!");
			}	
		});

		var searchItem = $('#searchedItem').text();
		if(searchItem.length > 0){
			$('.main-banner').css('display','none');
			$('.search-result').css('display','block');
		}else{
			$('.main-banner').css('display','block');
			$('.search-result').css('display','none');
		}

		/*Login*/	
		$('input[name=login]').click(function(e){
			e.preventDefault();
			var email = $('input[name=email_login]').val();
			var pwd = $('input[name=password_login]').val();
			var itemId = $('input[name=itemid]').val();
			var nFeature = $('input[name=nfeature]').val();

			$.ajax({
				url:'processaccount.php',
				method:'POST',
				data:{email:email,password:pwd,action:'login'},
				beforeSend: function(){					
					$('input[name=login]').val("Logging in...");
					$('input[name=login]').attr('disabled', 'disabled');
					setTimeout(function(){
						$('input[name=login]').val("Login");
						$('input[name=login]').removeAttr('disabled');
					},2000);
				},
				success: function(data){
					//alert(data);
					if(data == 1){
						sessionStorage.setItem('status','loggedIn');

						if(nFeature != ""){
							window.location.replace('mybooks.php?n='+nFeature+'&&id='+itemId);
						}else{
							window.location.replace('home.php');
						}
					}else{
						$('.error').text(data);
						$('.error').css("display","block");
						setTimeout(function(){
							$('.error').css("display","none");
						},7000);						
					}
				},
				error: function(a,b,c){

				}
			});
		});			
});