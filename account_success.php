<?php
require_once 'vendor/autoload.php';

use App\Templates\IndexTemplate;

$indexTemplate = new IndexTemplate();

$indexTemplate->page = "Registration success";
$indexTemplate->content = "
	<div class='content'>
		<div class='registration-success-page'>
			<div>
				<div class='img'><img src='app_img/logo.png'/></div>
				<p>Welcome to bukswap, your account was successfully created.</p>
			</div>
			<div>
				<h4>Final step.</h4>
				<p>An activication link was sent to your email.
				Go to your email, click it to  activate your account</p>
			</div>
			<div>
				<h4>Login</h4>
				<p>Only login once you have activated your account.</p>
				<div class='proceed-btn'>
					<a href='account_credentials.php'>Proceed to login</a>
				</div>
			</div>
		</div>	
	</div>
";
$indexTemplate->page = "registration successful";
echo $indexTemplate->header();
echo $indexTemplate->content();
echo $indexTemplate->footer();
?>
