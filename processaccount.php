<?php
require_once 'vendor/autoload.php';

use App\Middlewares\Registration;
use App\Middlewares\Login;

$errorLogin = "";

if($_POST['action'] == 'login'){
	$login = new Login();

	$login->captureData();

	if($login->validateData()){
		$errorLogin = $login->validateData();
	}else{			
		$errorLogin =$login->utilizeData();
	}
	echo $errorLogin;
}