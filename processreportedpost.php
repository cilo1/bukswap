<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessReportedPost;

$processReportedPost = new ProcessReportedPost();
echo $processReportedPost->utilizeData();