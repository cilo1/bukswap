<?php
require_once 'vendor/autoload.php';

use App\Middleware\DeactivateAccount;

$deactivateAccount = new DeactivateAccount();
$deactivateAccount->beginSession();
$deactivateAccount->authenticateSession();
echo $deactivateAccount->utilizeSession();