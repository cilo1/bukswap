<?php
	require_once 'vendor/autoload.php';

	use App\Middleware\MyBooks;
	use App\Templates\MyBooksTemplate;
	use App\Templates\IndexTemplate;
	use App\Middleware\ProcessBooklist;
	use App\Middleware\ProcessWatchlist;

	$result = array();

	if(!isset($result[0])){
		$result[0] = null;
	}

	$myBooks = new MyBooks();
	$myBooks->beginSession();
	$myBooks->authenticateSession();				

	$indexTemplate = new IndexTemplate();
	$myBooksTemplate = new MyBooksTemplate();

	if(isset($_GET['item'])){
		$myBooksTemplate->deleteItem = array($_GET['item'],$_GET['a']);
	}

	if(isset($_GET['n'])){
		$nFeature = $_GET['n'];

		if($nFeature == "booklist"){
			$processBooklist = new ProcessBooklist();
			$processBooklist->utilizeSession();
		}

		if($nFeature == "watchlist"){
			$processWatchlist = new ProcessWatchlist();
			$processWatchlist->utilizeSession();
		}
	}

	/*
	* my books
	*/
	$mybooksResult = $myBooks->myBooks();
	if($mybooksResult[1] == false){
		$mybooksResultSize = 0;
		$mybooksResultItems = null;
	}else{
		$mybooksResultItems = $mybooksResult[1];
		$mybooksResultSize = count($mybooksResultItems);						
	}
	$myBooksTemplate->myBooks = $mybooksResultItems;
	$myBooksTemplate->myBooksSize = $mybooksResultSize;

	/*
	* my booklist
	*/
	$mybooklistResult = $myBooks->myBooklist();		
	if($mybooklistResult[1] == false){
		$mybooklistResultSize = 0;
		$mybooklistResultItems = null;
	}else{
		$mybooklistResultItems = $mybooklistResult[1];
		$mybooklistResultSize = count($mybooklistResultItems);						
	}
	$myBooksTemplate->myBooklistItems = $mybooklistResultItems;
	$myBooksTemplate->myBooklistSize = $mybooklistResultSize;

	/*
	* my watchlist
	*/
	$mywatchlistResult = $myBooks->myWatchlist();		
	if($mywatchlistResult[1] == false){
		$mywatchlistResultSize = 0;
		$mywatchlistResultItems = null;
	}else{
		$mywatchlistResultItems = $mywatchlistResult[1];
		$mywatchlistResultSize = count($mywatchlistResultItems);						
	}
	$myBooksTemplate->myWatchlistItems = $mywatchlistResultItems;
	$myBooksTemplate->myWatchlistSize = $mywatchlistResultSize;

	$indexTemplate->page = "my books";
	echo $indexTemplate->header();
	//$indexTemplate->content();
	$myBooksTemplate->displayPageContent();
	echo $indexTemplate->footer();
?>