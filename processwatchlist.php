<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessWatchlist;

$processWatchlist = new ProcessWatchlist();
$processWatchlist->beginSession();

if($processWatchlist->authenticateSession() == false){
	echo "auth failed"; 
}else{
	echo $processWatchlist->utilizeSession();
}
