<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessNotification;

$processNotification = new ProcessNotification();
$processNotification->beginSession();
$processNotification->authenticateSession();
echo $processNotification->utilizeSession();