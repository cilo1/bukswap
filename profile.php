<?php
require_once 'vendor/autoload.php';

use App\Middlewares\Profile;
use App\Templates\IndexTemplate;
use App\Templates\MyProfileTemplate;

$profile = new Profile();
$profile->beginSession();
$profile->authenticateSession();

$message = [];

$indexTemplate = new IndexTemplate();
$myProfileTemplate = new MyProfileTemplate();

if(isset($_POST['submit'])){
	$profile->captureData();
	$message = $profile->validateData();

	if(empty($message[1])){
		$message = $profile->utilizeData();
	}	

	if($message[0] == "error" && !empty($message[1])){
		$myProfileTemplate->message = "<div class='error'>".$message[1]."</div>";
	}
	if($message[0] == "success"){
		$myProfileTemplate->message = "<div class='success'>".$message[1]."</div>";
	}
}
$result = $profile->utilizeSession();
$myProfileTemplate->sellerData = $result;
$indexTemplate->page = "my profile";
$indexTemplate->content = $myProfileTemplate->displayPageContent();

echo $indexTemplate->header();
echo $indexTemplate->content();
echo $indexTemplate->footer();
	
?>		
		