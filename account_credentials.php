<?php
	require_once 'vendor/autoload.php';

	use App\Templates\AccountTemplate;
	use App\Templates\IndexTemplate;
	use App\Middlewares\Registration;
	use App\Middlewares\Login;
	use App\Classes\Form;

	$form = new Form();
	$indexTemplate = new IndexTemplate();
	$accountTemplate = new AccountTemplate();

	//$token = $form->generatedFormToken();	

	if(isset($_POST['register'])){
				
		$registration = new Registration();

		$registration->captureData();

		if($registration->validateData()){
			$accountTemplate->errorReg = $registration->validateData();	
		}else{				
			$registration->utilizeData();	
		}
	}

	if(isset($_POST['login'])){
		$login = new Login();

		$login->captureData();

		if($login->validateData()){
			$accountTemplate->errorLogin = $login->validateData();
		}else{			
			$accountTemplate->errorLogin =$login->utilizeData();
		}
	}

	if(isset($_GET['n'])){
		$accountTemplate->itemId = $_GET['id'];
		$accountTemplate->nFeature = $_GET['n'];
	}
	
	$indexTemplate->page = "account credentials";	
	$indexTemplate->header();
	$accountTemplate->displayPageContent();
	$indexTemplate->footer();
?>		
		