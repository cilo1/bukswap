<?php
require_once 'vendor/autoload.php';
use App\Templates\ForgotPasswordTemplate;
use App\Templates\IndexTemplate;
use App\Middleware\ForgotPassword;

$forgotPassword = new ForgotPassword();
$forgotPasswordTemplate = new ForgotPasswordTemplate();
$indexTemplate = new IndexTemplate();

if(isset($_POST['submit'])){
	$forgotPassword->captureData();
	$error = $forgotPassword->validateData();

	if(empty($error)){

		if($forgotPassword->utilizeData()){
			$forgotPasswordTemplate->message = "<div class='success'>Your email was sent successfully. Check your email for more instructions.</div>";
		}else{
			$forgotPasswordTemplate->message = "<div class='error'>This process failed. Try again later!</div>";
		}
	}else{
		$forgotPasswordTemplate->message = "<div class='error'>".$error."</div>";
	}
}

$indexTemplate->page = "forgot password";
$indexTemplate->header();
$forgotPasswordTemplate->displayPageContent();
$indexTemplate->footer();