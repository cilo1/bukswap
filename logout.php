<?php
require_once 'vendor/autoload.php';

use App\Middlewares\Logout;

$logout = new Logout();

$logout->beginSession();
$logout->authenticateSession();
$logout->utilizeSession();

