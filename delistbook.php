<?php
require_once 'vendor/autoload.php';

use App\Middleware\DelistBook;
use App\Templates\IndexTemplate;

$indexTemplate = new IndexTemplate();
$delistBook = new DelistBook();

$delistBook->beginSession();
$delistBook->authenticateSession();

if($delistBook->utilizeSession() == true){
	$message = "<div class='success'>The book was successfully delisted!</div>";
}else{
	$message = "<div class='error'>Sorry the book was was not delisted!</div>";
}
$content = "
	<div class='content'>
	".$message."
		<div class='delistedbookFeedback'>
			<p>				 
				<div class='proceed-btn'>
					<a href='mybooks.php'>Proceed to my books</a>
				</div>
			</p>
		</div>	
	</div>
";
$indexTemplate->page = "delist book";
$indexTemplate->content = $content;
echo $indexTemplate->header();
echo $indexTemplate->content();
echo $indexTemplate->footer();
