<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessBookDelisting;

$processBookDelisting = new ProcessBookDelisting();
$processBookDelisting->beginSession();
$processBookDelisting->authenticateSession();
echo $processBookDelisting->utilizeSession();