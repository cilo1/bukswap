<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessBooklist;

$processBooklist = new ProcessBooklist();
$processBooklist->beginSession();

if($processBooklist->authenticateSession() == false){
	echo "auth failed"; 
}else{
	echo $processBooklist->utilizeSession();
}