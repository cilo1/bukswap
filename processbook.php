<?php
require_once 'vendor/autoload.php';

use App\Middleware\ProcessBook;

$processBook = new ProcessBook();
$processBook->beginSession();
$processBook->authenticateSession();
echo $processBook->utilizeSession();