<?php
	require_once 'vendor/autoload.php';
	
	use App\Middleware\VerifyAccount;

	$verifyAccount = new VerifyAccount();
	$verifyAccount->captureData();
	$verifyAccount->validateData();
	$verifyAccount->utilizeData();
?>
<html>
	<head>
		<title></title>
	</head>
	<style>
	.content{
		padding:1%;
	}
	</style>
	<body>
		<div class="content">
			<h2>Account verification failed!</h2>
			<p>
			Your account was not verified, Click the link below to register a new account.			
			<p>
			<p>
				<a href="www.codevated.com/app/views/account_credentials.php">Registration page</a>
			</p>
		</div>
	</body>
</html>