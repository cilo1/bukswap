<?php
namespace App\Templates;

class PostBookTemplate{

	public $result;

	public function displayPageContent(){
		echo "
		<div class='content'>
			<div class='post-book-content'>
				<h3>Post a book</h3>";

					if(!empty($this->result[0])){		
						echo $this->result[1];
					}
		echo "		
				<form action='".$_SERVER['PHP_SELF']."' method='post' enctype='multipart/form-data'>
					<div class='book-info'>
						<h4>Book bio</h4>
						<div>									
							<input name='fileToUpload' type='file' />
						</div>
						<div>
							<input name='title' type='text' placeholder='Book title' maxlength='100'/>
						</div>
						<div>
							<input name='publisher' type='text' placeholder='Book publisher' maxlength='100'/>
						</div>
					</div>

					<div class='book-residence'>
						<h4>Target group:</h4>
						<div>
							<select name='country'>
								<option>Country</option>
								<option>Kenya</option>
							</select>
						</div>
						<div>
							<select name='education-level'>
								<option>Education level</option>
								<option>Primary</option>
								<option>Secondary</option>
							</select>
						</div>
						<div>
							<select name='class'>
								<option>Class</option>
							</select>
						</div>
						<div>
							<input name='subject' type='text' placeholder='Subject' maxlength='100'/>
						</div>
					</div>
					
					<div class='book-status'>
						<h4>Book status:</h4>
						<div>
							<input name='price' type='text' placeholder='Price' maxlength='5'/>
						</div>
						<div>
							<textarea cols='20' rows='6' name='condition' placeholder='Book condition'></textarea>
						</div>
						<div>
							<input name='swap' type='checkbox'/> Agree to a book swap.
						</div>
					</div>					
					<div class='clear'></div>
					<div>
						<input name='submit' type='submit' value='Post book'/>		
					</div>
				</form>
			</div>							
		</div>
		";
	}
}