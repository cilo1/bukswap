<?php

namespace App\Templates;

use App\Templates\Template;
use App\Interfaces\TemplateMethods;


class IndexTemplate extends Template implements TemplateMethods{
	public $content;

	public function header(){
		$this->headerTemplate();
	}

	public function content(){
		return $this->content;
	}

	public function footer(){
		$this->footerTemplate();
	}

}