<?php
namespace App\Templates;

class AccountTemplate{

	public $token;
	public $errorReg;
	public $errorLogin;
	public $success;

	//managing features that require account logins
	public $itemId;
	public $nFeature;

	public function __construct(){}

	public function displayPageContent(){
			echo "
			<div class='content'>
				<div class='seller-forms'>
					<h3>Account credentials</h3>
					<p>You must have an account to post a book on bukswap.</p>
					<div class='login'>
						<h4>Login</h4>
				<form action='".$_SERVER['PHP_SELF']."'' method='post'>
					<input type='text' name='token' value='".$this->token."' />
					<input type='hidden' name='itemid' value='".$this->itemId."' />
					<input type='hidden' name='nfeature' value='".$this->nFeature."' />
					<div>
						<input name='email_login' type='text' value='".$_POST['email']."' placeholder='Enter your email'/>	
					</div>
					<div>
						<input name='password_login' type='password' placeholder='Password'/>	
					</div>
					<div>
						<span><a href='forgotpassword.php'>Forgot password?</a></span>
						<input name='login' type='submit' value='Login'/>		
					</div>	
				</form>
				<div class='mobile-account-form-switch'>
					Create new account
				</div>
				</div>
				<div class='signup'>
					<h4>Sign Up</h4>";
					if(!empty($this->errorReg)){
						echo "<div class='error'>".$this->errorReg."</div>";
					}

					if(!empty($this->success)){
						echo "<div class='success'>".$this->success."</div>";
					}
			echo "<form action='".$_SERVER['PHP_SELF']."' method='post'>
					<input type='text' name='token' value='".$this->token."'/>
					<div>
						<input name='fname' value='".$_POST['fname']."' type='text' placeholder='Firstname'/>		
					</div>
					<div>
						<input name='lname' value='".$_POST['lname']."' type='text' placeholder='Lastname'/>		
					</div>
					<div>
						<input name='email' value='".$_POST['email']."' type='text' placeholder='Email'/>		
					</div>						
					<div>
						<input name='phone' value='".$_POST['phone']."' type='text' placeholder='Phone number'/>	
					</div>
					<div>
						<input name='password' type='password' placeholder='Password'/>	
					</div>
					<div>
						<input name='password2' type='password' placeholder='Confirm password'/>
					</div>
					<div>
						<input name='location' value='".$_POST['location']."'' type='text' placeholder='Where are you based (location)'/>		
					</div>
					<div>
						<select name='country'>
							<option value='0'>Country</option>
							<option>Kenya</option>
							<option>Uganda</option>
							<option>Tanzani</option>
							<option>Rwanda</option>
							<option>Nigeria</option>
						</select>		
					</div>
					<div>
						<input name='register' type='submit' value='Create account'/>		
					</div>
				</form>
				<div class='mobile-account-form-switch'>
					Login to account
				</div>
				</div>
				<div class='clear'></div>
				</div>
			</div>	
		";
	}
}