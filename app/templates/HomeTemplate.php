<?php
namespace App\Templates;

use App\Models\SellerModel;
use App\Classes\Seller;
use App\Models\BooklistModel;
use App\Classes\Booklist;
use App\Models\WatchlistModel;
use App\Classes\Watchlist;
use App\Classes\LinkManager;

class HomeTemplate {

	private $sellerModel;
	private $seller;
	private $booklistModel;
	private $booklist;
	private $watchlistModel;
	private $watchlist;
	private $linkmanager;

	public $searchItem;
	public $result;


	private $searchListSize;
	private $searchlist;

	public function __construct(){
		$this->sellerModel = new SellerModel();
		$this->seller = new Seller();
		$this->booklistModel = new BooklistModel();
		$this->booklist = new Booklist();
		$this->watchlistModel = new WatchlistModel();
		$this->watchlist = new Watchlist();
		$this->linkmanager = new Linkmanager();
	}

	public function displayPageContent(){		

		echo "
		<div class='content'>
			<div class='jumbotron'>
				<div class='search'>
					<p>Find a book:</p>";				

					if($this->result[0] == 'error'){		
						echo $this->result[1];
					}
					
		echo "
		<form action='".$_SERVER['PHP_SELF']."' method='post'>
			<div>
				<input name='item' type='text' value='".$this->searchItem."' placeholder='Search book by title...'/>

				<div class='submit-search-btn'>
					<input name='search' type='submit' value='Search'/>
				</div>
				<div class='clear'></div>
			</div>				
		</form>
		</div>";

		if($this->result[0] == "success"){
			
			if($this->result[1] == false){
				$this->searchListSize = 0;
				$this->searchlist = null;
			}else{
				$this->searchlist = $this->result[1];
				$this->searchListSize = count($this->searchlist);	
			}
			$this->searchResults();
		}	

		echo "
			<div class='main-banner'>
				<div class='banner'>
					<h2>Create more room by selling your used books to others</h2>
					<p>There are pleanty of people willing pay for them on bukswap.</p>
				</div>
				<ul>
					<h3>Bukswap in a nutshell:</h3>
					<li>
						<img src='app_img/sell.png'/>
						<p><span>Sell</span> used books to make extra cash.</p>
					</li>
					<li>
						<img src='app_img/buy.png'/>
						<p><span>Buy</span> used books to save more money.</p>
					</li>
					<li>
						<img src='app_img/swap.png'/>
						<p><span>Swap</span> used books with other users.</p>
					</li>
					<div class='clear'></div>
				</ul>
			</div>					
			</div>
		</div>
		";
	}

	public function searchResults(){		
		echo "
		<div class='search-result'>
			<div class='search-result-title'>
			<h4>
				Search results: ".$this->searchListSize." 
				items found for <span id='searchedItem'>".ucfirst($this->searchItem)."</span>
			</h4>";
			
			if(isset($_SESSION['seller_id'])){
				$this->watchlist->setBookTitle($this->searchItem);
				$this->watchlist->setSellerId($_SESSION['seller_id']);
				$this->watchlistModel->setData($this->watchlist);
			}			

			if($this->watchlistModel->checkIfBookTitleAlreadyExists() == true){
				echo "
					<div class='remove-from-watchlist-btn'>
						X Remove from watchlist
					</div>
				";
			}else{
				$this->searchItem = \base64_encode($this->searchItem);
				echo "<div class='add-to-watchlist-btn'>
					<a href='".$this->searchItem."'>+ Add book to watchlist</a>
				</div>";
			}
			
		echo "
			<div class='clear'></div>
			</div>";

		if($this->searchListSize > 0){

			echo "<div class='search-result-content'>";

				foreach($this->searchlist as $item){
					$this->seller->setSellerId($item['seller_id']);
					$this->sellerModel->setdata($this->seller);

					$sellerData = $this->sellerModel->findSellerById();

					$sellerName = $sellerData['fname']." ".$sellerData['lname'];

					$bookIdEncoded = $this->linkmanager->encodeUrlId($item['book_id']);

					$this->booklist->setBookId($item['book_id']);

					if(isset($_SESSION['seller_id'])){
						$this->booklist->setSellerId($_SESSION['seller_id']);
					}
					$this->booklistModel->setData($this->booklist);

					echo "<div class='book-item'>
						<div class='book-item-img'>
							<img src='".$item['book_img']."'/>
						</div>
						<div class='book-item-info'>
							<h4>
								".$item['book_title'].", 
							</h4>
							<p>
								<span>Level:</span> 
								".$item['education_level'].", 
								<span>Subject:</span> 
								".$item['subject']."
							</p>
							<p>
								<span>Seller:</span> 
								<a href=''>".$sellerName."</a>, 
								<span>Location:</span>
								".$sellerData['location'].", 
								<span>Phone:</span> 
								".$sellerData['phone']."
							</p>
							<p>
								<div class='did-you-find-btn'>
									<a class='did-you-find-link' href='".$bookIdEncoded."'>Did you find this book from 
									".$sellerName."?</a>								
								</div>
								<div class='did-you-find-response'>
									<ul>
 										<li><a class='response-link' href='".$bookIdEncoded."'>Yes</a></li>
 										<li><a class='response-link' href='".$bookIdEncoded."'>No</a></li>
 										<li><a class='close-link' href=''>X close</a></li>
 										<div class='clear'></div>
									</ul>
								</div>
							</p>
						</div>
						<div class='book-item-more-info'>";
								if($this->booklistModel->checkIfBookAlreadyExists() == true){
									echo "									
									<div class='unbookmark-img'>
										<a href='".$bookIdEncoded."' class='bookmark-link'>
										<img src='app_img/book-marked.png' id='img'/>
										<p>remove from booklist</p>
										</a>						
									</div>
									";
								}else{
									echo "
									<div class='bookmark-img'>							
										<a href='".$bookIdEncoded."' class='bookmark-link'>
										<img src='app_img/book-unmarked.png' id='img'/>
										<p>add to booklist</p>
										</a>						
									</div>
									";
								}									
							echo "	
							<p>
								<div class='report-post-btn'>
									<a href='".$bookIdEncoded."'>Report this post</a>
								</div>
								<div class='report-post-form'>
									<div class='report-post-form-header'>
										<h4>What is wrong with this post?</h4>
										<div class='close-btn'>Xclose</div>
										<div class='clear'></div>
									</div>
									<div>
									<input name='report-post' type='radio' value='It is not a book'/>
									It is not a book
									</div>
									<div>
									<input name='report-post' type='radio' value='It is a spam'/>
									It is a spam
									</div>
									<div>
									<input name='report-post' type='radio' value='It is offensive'/>
									It is offensive
									</div>
									<div>
									<input name='report-post' type='radio' value='It is sensitive'/>
									Displays a sensitive image
									</div>
									<div>
									<input name='' class='send-report-btn' type='submit' value='Send report'/>
									</div>
								</div>
							</p>
						</div>
							<div class='clear'></div>
					</div>";
				}	
			}else{
				echo "<div class='not-found'>No book found!</div>";
		}									
		echo "</div>";
	}						
}