<?php

namespace App\Templates;

use App\Traits\Session;

class Template{
	public $page;

	use Session;

	/*
	*	Array values for javascript and css links
	*/
	public $jscripts = array(
					"<script
					  src='http://code.jquery.com/jquery-3.3.1.js'
					  integrity='sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60='
					  crossorigin='anonymous'></script>",
					"<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js' integrity='sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q' crossorigin='anonymous'></script>",
					"<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js' integrity='sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4' crossorigin='anonymous'></script>",
					"<script src='js/jscript.js'></script>"
					);

	public $css = array(
					"<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css' integrity='sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy' crossorigin='anonymous'>",
					"<link rel='stylesheet' href='css/style.css'>"

					);

	public function scripts(){

		foreach($this->jscripts as $link){
			echo $link;
		}	

	}

	public function styles(){

		foreach($this->css as $link){
			echo $link;
		}
	}

	public function headerTemplate(){
		$this->startSession();

		echo "
			<!DOCTYPE html>
			<html>
			<head>
			<meta charset='UTF-8'>
			<meta name='viewport' content='width=device-width, initial-scale=1.0'>	
			";
			$this->scripts();
			$this->styles();						
		echo "
			<title>".$this->page."</title>
			</head>
			<div class='notify'></div>
			<div class='dark-popup-background'></div>
			<body>
				<header>
					<div class='logo'>
						<a href='index.php'>
							<img src='app_img/logo.png'/>
						</a>	
					</div>";

					if($this->authenticateSessionData() == true){
						echo "
						<div class='login-menu'>
							<ul>								
								<li>
									<div class='how-it-works-btn'><a href=''>How it works</a></div>
								</li>";
								if($this->page == "home"){
									echo "<li class='current'><a href='home.php'>home</a></li>";
								}else{
									echo "<li><a href='home.php'>home</a></li>";
								}
								if($this->page == "mybooks"){
									echo "<li class='current'><a href='mybooks.php'>my books</a></li>";
								}else{
									echo "<li><a href='mybooks.php'>my books</a></li>";
								}								
								echo "								
								<li>
									<a href='' class='notification-link'>
									<span id='notification-menu-pop'></span>
									<img src='app_img/notification.png'/>
									</a>
									<div class='notification-content-pop'>
										<div class='notification-content-header'>
											<h4>My notifications</h4>
											<div class='close-btn'>Xclose</div>
											<div class='clear'></div>
										</div>
										<div class='notification-content-info'>
											
										</div>
									</div>
								</li>
								<li>
									<a href='' class='account-link'>
										<img src='app_img/account.png'/>
									</a>
									<div class='account-content-pop'>
									<ul>
										<li>
											<a href='profile.php'>My profile</a>
										</li>			
										<li>
											<a href='logout.php'>logout</a>
										</li>
									</ul>
									</div>
								</li>
								<li>
								 	<div class='post-book-btn'><a href='postbook.php'>Post a book</a></div>
								</li>
								<div class='clear'></div>	
							</ul>
						</div>
						<div class='menu-mobile'>
							<img src='app_img/menu.png'/>						
						</div>
						<div class='menu-mobile-content'>
							<div class='menu-mobile-content-close-btn'>
								<img src='app_img/remove.png'/>						
							</div>
							<ul>
								<li><a href='home.php'>home</a></li>
								<li>
								 	<div class='post-book-btn'><a href='postbook.php'>Post a book</a></div>
								</li>
								<li><a href='mybooks.php'>My books</a></li>	
								<li><a href='profile.php'>My profile</a></li>			
								<li><a href='logout.php'>logout</a></li>				
							</ul>
						</div>
						";
					}else{
						echo "
						<div class='menu'>
							<ul>
								<li>
									<div class='how-it-works-btn'><a href=''>How it works</a></div>
								</li>
								<li><a href='index.php'>Home</a></li>	
								<li><a href='account_credentials.php'>Account</a></li>	
								<li>
								 	<div class='post-book-btn'><a href='account_credentials.php'>Post a book</a></div>
								</li>
								<div class='clear'></div>	
							</ul>
						</div>
						<div class='menu-mobile'>
							<img src='app_img/menu.png'/>						
						</div>
						<div class='menu-mobile-content'>
							<div class='menu-mobile-content-close-btn'>
								<img src='app_img/remove.png'/>						
							</div>
							<ul>
								<li>
									<div class='how-it-works-btn'><a href=''>How it works</a></div>
								</li>
								<li><a href='index.php'>Home</a></li>	
								<li><a href='account_credentials.php'>Account</a></li>	
								<li>
								 	<div class='post-book-btn'><a href='account_credentials.php'>Post a book</a></div>
								</li>	
							</ul>
						</div>														
						";						
					}					
				echo "									
					<div class='clear'></div>
				</header>
				<div class='error'></div>
				<div class='must-be-loggedin-popup'>
					<div class='must-be-loggedin-popup-header'>
						<h4>Must be logged in</h4>
						<div class='close-btn'>X</div>
						<div class='clear'></div>
					</div>
					<p>
					To proceed with this process you must be logged in.
					</p>
					<p>
						<div class='proceed-btn-green'>
							<a href=''>Proceed to accounts</a>
						</div>
						<div class='proceed-btn-dark'>
							<a href=''>Not interested</a>
						</div>
					</p>
				</div>
				";
	}

	public function footerTemplate(){
		echo "
			<footer>
			<p>
				<ul>
					<li>&copy; 2018 bukswap</li>
					<li><a href=''>About</a></li>
					<li><a href=''>Privacy and policy</a></li>
					<li><a href=''><img src='app_img/twitter.png'/></a></li>
					<li><a href=''><img src='app_img/fb.png'/></a></li>
					<div class='clear'></div>
				</ul>
			</p>
			</footer>

		";
	}
}