<?php
namespace App\Templates;

class ForgotPasswordTemplate{

	public $message;

	public function displayPageContent(){
		echo "
			<div class='content'>
			".$this->message."
				<div class='forgot-password-page'>
				<h3>Forgot Password, let us find your account</h3>
				<form action='".$_SERVER['PHP_SELF']."' method='post'>
					<div>
					<input name='email' type='text' placeholder='Enter your email address'/>
					</div>
					<div>
					<input name='submit' type='submit' value='Submit'/>
					</div>
				</form>
				</div>				
			</div>
		";
	}
}