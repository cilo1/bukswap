<?php

namespace App\Templates;

use App\Classes\LinkManager;
use App\Models\BookModel;
use App\Classes\Book;

class MyBooksTemplate{
	private $linkManager;
	private $bookModel;
	private $book;

	/*
	* deleteItem: array with two values
	*/
	public $deleteItem;

	public $myBooks;
	public $myBooksSize;

	public $myBooklistItems;
	public $myBooklistSize;

	public $myWatchlistItems;
	public $myWatchlistSize;

	public function __construct(){
		$this->linkManager = new LinkManager();
		$this->book = new Book();
		$this->bookModel = new BookModel();
	}

	public function displayPageContent(){
		echo "			
			<div class='content'>
				<div class='confirmation-box'>
					<h4>Confirm action</h4>
						<p>
						Are you sure you want to delete from <span></span>	
						<h5></h5> 	
						</p>
						<div>
							<ul>
								<li class='cancel'>Cancel</li>
								<li class='yes'>
									<a href=''>Yes</a>
								</li>
							</ul>
							<div class='clear'></div>
						</div>
				</div>				
				<div class='books'>
					<div class='left'>						
					<div class='mybooks'>
					<h4>My books: (".$this->myBooksSize.")</h4>";

						if($this->myBooksSize > 0){

							foreach($this->myBooks as $item){
								$bookId = $this->linkManager->encodeUrlId($item['book_id']);

								echo "
									<div class='mybooks-item'>
										<div class='mybooks-item-img'>
											<img src='".$item['book_img']."'/>
										</div>
										<div class='mybooks-item-info'>
											<h5>
												".$item['book_title'].", 
											</h5>
											<p>
												<span>Level:</span> ".$item['education_level'].",
											</p> 
											<p>
												<span>Subject:</span> ".$item['subject']."
											</p>								
										</div>
										<div class='clear'></div>
										<div class='action-btns'>
											<ul>
												<li><a href='editbook.php?item=".$bookId."'>Edit</a></li>
												<li><a href='".$bookId."' class='delete-link'>Delete</a></li>
											</ul>
										</div>
									</div>";
								}
								echo "<div class='clear'></div>";
							}else{
								echo "<div class='no-book-posted'>
									No books yet. <a href='postbook.php'>Post your first book</a>
									</div>";	
							}				

					echo "</div>	

					</div>
					<div class='right'>
						<div class='mywatchlist'>
							<h4>My watchlist (".$this->myWatchlistSize.")</h4>";
							if($this->myWatchlistSize > 0){
								
								foreach($this->myWatchlistItems as $watchlistItem){

									$watchlistId = $this->linkManager->encodeUrlId($watchlistItem['watchlist_id']);
															
									echo "
									<div class='mywatchlist-item'>
										<div class='mywatchlist-item-info'>
											<a href='".$watchlistId."'>".$watchlistItem['book_title']."</a>				
										</div>
										<div class='mywatchlist-item-actions'>
											<ul>
												<li>
													<span>Total: 1.9k</span>
												</li>
												<li>
													<a class='remove-watchlist-btn' href='".$watchlistId."'>
															<img src='app_img/remove.png'/>
													</a>
												</li>
											</ul>
										</div>
									</div>";
								}
							}else{
								echo "<p>No books yet!</p>";
							}
						echo "
						</div>
						<div class='mywatchlist-show-more-btn'>show more</div>
						<div class='mybooklist'>
							<h4>My booklist(".$this->myBooklistSize.")</h4>";
							if($this->myBooklistSize > 0){								
								
								foreach($this->myBooklistItems as $booklistItem){
									$this->book->setBookId($booklistItem['book_id']);
									$this->bookModel->setData($this->book);
									$booklist = $this->bookModel->findBookById();
									$bookId = $this->linkManager->encodeUrlId($booklist['book_id']);
															
									echo "
									<div class='mybooklist-item'>
										<div class='mybooklist-item-info'>
											<h5><a href='".$bookId."'>".$booklist['book_title']."</a></h5>
											<p>Cecil, 0718 485 173</p>
										</div>
										<div class='mybooklist-item-actions'>
											<ul>
												<li>
													<a class='remove-booklist-btn' href='".$bookId."'>
															<img src='app_img/remove.png'/> 
													</a>
												</li>
											</ul>
										</div>
									</div>
									";
								}
							}else{
								echo "<p>No books yet!</p>";
							}
						echo "
						</div>
						<div class='mybooklist-show-more-btn'>show more</div>
					</div>
					<div class='clear'></div>
				</div>
			</div>";
	}	
}