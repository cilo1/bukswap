<?php

namespace App\Templates;

class EditBookTemplate{

	public function displayEditBookForm($bookData,$bookId){
		echo "
			<div class='post-book'>
				<h3>Edit book</h3>
				<form action='".$_SERVER['PHP_SELF']."' method='post' enctype='multipart/form-data'>
					<input name='bookid' value='".$bookId."' type='text'/>
					<div class='book-info'>
						<h4>Book bio</h4>
						<div>Current book image:</div>
						<div class='book-info-img'>
							<img src='".$bookData['book_img']."'/>	
						</div>
						<div>Upload new book image:</div>
						<div>									
							<input name='fileToUpload' type='file' />
						</div>
						<div>Book title:</div>
						<div>
							<input name='title' type='text' value='".$bookData['book_title']."' placeholder='Book title' maxlength='100'/>
						</div>
						<div>Publisher:</div>
						<div>
							<input name='publisher' type='text' value='".$bookData['publisher']."' placeholder='Book publisher' maxlength='100'/>
						</div>
					</div>

					<div class='book-residence'>
						<h4>Target group:</h4>
						<div>Country:</div>
						<div>
							<select name='country'>
								<option>".$bookData['country']."</option>
								<option>Country</option>
								<option>Kenya</option>
							</select>
						</div>
						<div>Education level:</div>
						<div>
							<select name='education-level'>
								<option>".$bookData['education_level']."</option>
								<option>Education level</option>
								<option>Primary</option>
								<option>Secondary</option>
							</select>
						</div>
						<div>
						<div>Class:</div>
							<select name='class'>
								<option>".$bookData['class']."</option>
								<option>Class</option>
							</select>
						</div>
						<div>Subject:</div>
						<div>
							<input name='subject' type='text' value='".$bookData['subject']."' placeholder='Subject' maxlength='100'/>
						</div>
					</div>
					
					<div class='book-status'>
						<h4>Book status:</h4>
						<div>Price:</div>
						<div>
							<input name='price' type='text' value='".$bookData['price']."' placeholder='Price' maxlength='5'/>
						</div>
						<div>Book condition:</div>
						<div>
							<textarea cols='20' rows='6' name='condition' placeholder='Book condition'>".$bookData['book_condition']."</textarea>
						</div>
						<div>";
						if($bookData['swap'] == 'y'){
							echo "<input name='swap' type='checkbox' checked/>"; 
						}else{
							echo "<input name='swap' type='checkbox'/>"; 
						}
						echo "
						Agree to a book swap.
						</div>
					</div>					
					<div class='clear'></div>
					<div>
						<input name='submit' type='submit' value='Edit book'/>		
					</div>
				</form>
			</div>							
		";
	}
}