<?php

namespace App\Templates;

class MyProfileTemplate{
	public $message;

	public $sellerData;
	public $notifactionData;


	public function displayPageContent(){
		return "
		<div class='confirmation-deactivation'>
			<h4>Confirm action</h4>
			<div class='error'></div>
			<p>
				Are you sure you want to deativate your account	 	
			</p>
			<p>
			<input name='pwd' type='password' placeholder='Enter Password'/>
			</p>
			<div>
				<ul>
					<li class='cancel'>Cancel</li>
					<li class='yes'>
						<a href=''>Yes</a>
					</li>
				</ul>
				<div class='clear'></div>
			</div>
		</div>
		<div class='content'>
			".$this->message."
			<div class='profile'>
				<div class='left'>
					".$this->profileForm()."
				</div>
				<div class='right'>
					<div class='account-deactivate'>
						<h4>Deactivate account:</h4>
						<p>
						If you no longer want to use bukswap, you can deactivate your account.
						</p>
						<div class='account-deactivate-btn'>
							<a href=''>Deactivate account</a>
						</div>
					</div>
				</div>

				<div class='clear'></div>
				
			</div>
		</div>";
	}

	public function profileForm(){
		return "
			<div class='bio'>
				<h4>Bio info:</h4>
				<form action='".$_SERVER['PHP_SELF']."' method='post'>
					<div>
						<input name='fname' type='text' value='".$this->sellerData['fname']."' placeholder='Firstname'/>	
					</div>
					<div>
						<input name='lname' type='text' value='".$this->sellerData['lname']."' placeholder='Lastname'/>	
					</div>							
					<div>
						<input name='location' type='text' value='".$this->sellerData['location']."' placeholder='Where are you based (location)'/>	
					</div>
					<div>
						<select name='country'>
							<option>".$this->sellerData['country']."</option>
					 		<option>Country</option>
						</select>		
					</div>	
					<div>
						<input name='submit' type='submit' value='Change bio'/>	
					</div>
				</form>
			</div>

			<div class='contacts'>
				<h4>Contact info:</h4>
				<form action='".$_SERVER['PHP_SELF']."' method='post'>
					<div>
						<input name='email' type='text' value='".$this->sellerData['email']."' placeholder='Email'/>		
					</div>						
					<div>
						<input name='phone' type='text' value='".$this->sellerData['phone']."' placeholder='Phone number'/>	
					</div>
					<div>
						<input name='submit' type='submit' value='Change contacts'/>	
					</div>
				</form>	
			</div>
			<div class='passwords'>
				<h4>Change password info:</h4>
				<form action='".$_SERVER['PHP_SELF']."' method='post'>
					<div>
						<input name='currentPassword' type='password' placeholder='Current password'/>	
					</div>
					<div>
						<input name='newPassword' type='password' placeholder='New password'/>	
					</div>
					<div>
						<input name='confirmNewPassword' type='password' placeholder='Confirm new password'/>
					</div>
					<div>
						<input name='submit' type='submit' value='Change password'/>	
					</div>
				</form>	
			</div>
		";
	}
}