<?php
namespace App\Templates;

class ChangeForgottenPasswordTemplate{
	public $id;
	public $message;

	public function displayPageContent(){
		echo "
		<div class='content'>
			".$this->message."
			<div class='forgot-password-page'>
			<h3>Change your password</h3>
			<form action='".$_SERVER['PHP_SELF']."' method='post'>
				<input name='id' type='hidden' value='".$this->id."'/>
				<div>
				<input name='password' type='password' placeholder='Enter new password'/>
				</div>
				<div>
				<input name='password2' type='password' placeholder='Confirm new password'/>
				</div>
				<div>
				<input name='submit' type='submit' value='Change password'/>
				</div>
			</form>
			</div>				
		</div>
		";
	}
} 