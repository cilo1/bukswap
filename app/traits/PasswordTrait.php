<?php

namespace App\Traits;

trait Password{
	public $password;

	public function setPassword($password){
		$this->password = $password;
	}

	public function validPassword(){
		if(empty($this->password)){
			throw new \Exception("Password cannot be empty!");
		}

		if(strlen($this->password) < 5){
			throw new \Exception("Password characters must be more than 5!");
		}

		if(!preg_match('/[^a-z]/i',$this->password) || is_numeric($this->password)){
			throw new \Exception("Password cannot be letters or numbers only!");
		}

		return $this->password;
	}

	public function validConfirmPassword($confirmPassword){
		if(empty($confirmPassword)){
			throw new \Exception("Confirm password is empty");
		}

		if($this->password != $confirmPassword){
			throw new \Exception("Confirm password does not match password!");
		}

		return $confirmPassword;
	}

	public function encryptPassword(){
		$options = ['cost' => 12];
		$this->password = password_hash($this->password, PASSWORD_DEFAULT, $options);

		return $this->password;
	}
}