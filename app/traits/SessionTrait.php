<?php

namespace App\Traits;

use App\Classes\Seller;

trait Session{

	public $seller;

	public function startSession(){			
		if (session_status() == PHP_SESSION_NONE){
		    session_start();
		}
	}

	public function initializeSessionData($seller){
		$this->seller = $seller;

		$_SESSION['seller_id'] = $this->seller->getSellerId();
		$_SESSION['fname'] = $this->seller->getFname();
		$_SESSION['lname'] = $this->seller->getLname();
		$_SESSION['email'] = $this->seller->getEmail();
		
	} 

	public function authenticateSessionData(){
		if(isset($_SESSION['seller_id'])){
			return true;
		}else{
			return false;
		}
	}

	public function destroySession(){
		unset($_SESSION['seller_id']);
		unset($_SESSION['fname']);
		unset($_SESSION['lname']);
		unset($_SESSION['email']);

		$this->seller = null;

		if(session_destroy()){
			return true;
		}
	}
} 