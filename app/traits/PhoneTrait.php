<?php

namespace App\Traits;

use App\Models\SellerModel;
use App\Classes\Seller;

trait Phone{
	public $number;

	public function setPhoneNumber($number){
		$this->number = $number;
	} 

	public function validPhoneNumber(){
		$this->sellerModel = new SellerModel();	
		$this->seller = new Seller();

		$regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";

		if(empty($this->number)){
			throw new \Exception("Phone cannot be empty!");
		}

		if(!preg_match($regex, $this->number)){
			throw new \Exception("Phone number is invalid!");
		}

		$this->seller->setPhone($this->number);
		$this->sellerModel->setData($this->seller);

		if($this->sellerModel->checkIfPhoneExists() == true){
			throw new \Exception("Phone number already exists!");
		}

		return $this->number;
	}
}