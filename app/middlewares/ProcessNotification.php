<?php

namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\InputValidator;
use App\Classes\LinkManager;
use App\Classes\Notification;
use App\Models\NotificationModel;

class ProcessNotification implements SessionMethods,InputData{

	private $inputValidator;
	private $linkManager;
	private $notification;
	private $notificationModel;

	private $bookTitle;

	use Session;

	public function __construct(){
		$this->inputValidator = new InputValidator();
		$this->linkManager = new LinkManager();
		$this->notification = new Notification();
		$this->notificationModel = new NotificationModel();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){			
		return $this->utilizeData();
	}

	public function captureData(){
		
	}

	public function validateData(){
		
	}

	public function utilizeData(){
		$this->notification->setNotificationRecipient($_SESSION['seller_id']);
		$this->notificationModel->setData($this->notification);
		
		if($_POST['action'] == "notification-pop"){
			
			$rows = $this->notificationModel->findAllSellerNotifications();
			if($rows == false){				
				return 0;
			}else{				
				return count($rows);
			}			
		}

		if($_POST['action'] == "notification-data"){
			
			$rows = $this->notificationModel->findAllSellerNotifications();

			if($rows == false){	
				return 0;
			}else{
				$content['notifications'] = array();			
				foreach($rows as $row){
					$data = array();
					$data['notification_message'] = $row['notification_message'];
					$data['date_created'] = $row['date_created'];
					array_push($content['notifications'],$data);
				}
				return json_encode($content);
			}				
		}	
	}

	public function clearData(){}
}