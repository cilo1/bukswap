<?php
namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\Validation;
use App\Classes\LinkManager;
use App\Classes\Book;
use App\Models\BookModel;
use App\Traits\Image;

class ProcessBook implements SessionMethods,InputData{
	private $validation;
	private $linkManager;
	private $book;
	private $bookModel;

	private $bookId;

	use Session;
	use Image;

	public function __construct(){
		$this->validation = new Validation();
		$this->linkManager = new LinkManager();
		$this->book = new Book();
		$this->bookModel = new BookModel();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}		
	}

	public function captureData(){
		if(isset($_POST['id'])){			
			$this->bookId = $this->linkManager->decodeUrlId($_POST['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}
	}

	public function utilizeData(){
		$this->book->setBookId($this->bookId);

		$this->bookModel->setData($this->book);
		
		if($_POST['action'] == "delete_mybooks"){

			$row = $this->bookModel->findBookById();
			$this->setImageTargetDirectoryAndFile($row['book_img']);

			if($this->deleteImage() == true){
				if($this->bookModel->deleteBookById() == true){
					return true;
				}else{			
					return false;
				}
			}else{
				return false; 
			}			
		}	
	}

	public function clearData(){}
}
