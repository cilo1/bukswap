<?php
namespace App\Middleware;

use App\Interfaces\InputData;
use App\Classes\InputValidator;
use App\Traits\Email;
use App\Classes\Validation;
use App\Classes\LinkManager;
use App\Classes\ReportedPost;
use App\Models\ReportedPostModel;
use App\Classes\Book;
use App\Models\BookModel;
use App\Models\SellerModel;
use App\Classes\Seller;
use App\Classes\Notification;
use App\Models\NotificationModel;

class ProcessReportedPost implements InputData{
	private $inputValidator;
	private $validation;
	private $linkManager;

	private $bookId;
	private $claim;
	private $bookSellerId;
	private $bookTitle;

	private $message;	
	private $row;

	private $reportedPost;
	private $reportedPostModel;
	private $book;
	private $bookModel;
	private $sellerModel;
	private $notification;
	private $notificationModel;

	use Email;

	public function __construct(){
		$this->inputValidator = new InputValidator();
		$this->validation = new Validation();
		$this->linkManager = new LinkManager();
		$this->reportedPost = new ReportedPost();
		$this->reportedPostModel = new ReportedPostModel();
		$this->book =  new Book();
		$this->bookModel = new BookModel();
		$this->seller = new Seller();
		$this->sellerModel = new SellerModel();
		$this->notification = new Notification();
		$this->notificationModel =  new NotificationModel();
	}	

	public function captureData(){
		if(isset($_POST['id'])){			
			$this->bookId = $this->linkManager->decodeUrlId($_POST['id']);
			$this->claim = $this->inputValidator->validateInput($_POST['claim']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}
	}

	public function utilizeData(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->reportPost();
		}else{
			return false;
		}		
	}

	public function reportPost(){
		$this->reportedPost->setBookId($this->bookId);
		$this->reportedPost->setClaim($this->claim);
		$this->reportedPostModel->setData($this->reportedPost);

		$this->book->setBookId($this->bookId);
		$this->bookModel->setData($this->book);
		$this->row = $this->bookModel->findBookById();
		$this->bookSellerId = $this->row['seller_id'];
		$this->bookTitle = $this->row['book_title'];

		$this->message = "<div>
				<p>
					A bukswap user has reported your book:
				</p> 
				<h3 style='color:#000000;text-align:left'>".$this->bookTitle.".</h3>
				<p> 
					The claim is as follows:
				</p> 
				<p>
					<b>".$this->claim."</b>
				</p>
				<p>
					If this claim is true proceed to delist it,
					<div style='border:#235cce solid 0.1em;background:#235cce;text-align:center;padding:1%;width:50%;margin-left:25%;border-radius:5px'>
						<a href='http://www.bukswap.com/delistbook.php?id=".$this->bookId."' style='font-size:14px;color:#ffffff;text-decoration:none'>
						Delist this book
						</a>
					</div>					
				</p>
				<p>
				Otherwise, send an email to info@bukswap.com for further instructions.
				</p>
				<p>Thank you in advance,</p> 
				<p>bukswap team.</p>  
			</div>";

		$this->seller->setSellerId($this->bookSellerId);
		$this->sellerModel->setData($this->seller);
		$this->row = $this->sellerModel->findSellerById();
		$this->email = $this->row['email'];
		$this->subject = "Bukswap: Your book has been reported";
		$this->body = $this->message;

		if($this->reportedPostModel->createReportedPost() == true){
			$this->notification->setNotificationRecipient($this->bookSellerId);
			$this->notification->setNotificationMessage($this->message);
			
			$this->notificationModel->setData($this->notification);

			if($this->notificationModel->createNotification() == true){
				$this->sendEmail();
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function clearData(){}
}