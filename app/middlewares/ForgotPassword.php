<?php

namespace App\Middleware;

use App\Classes\Form;
use App\Classes\Validation;
use App\Interfaces\InputData;
use App\Models\SellerModel;
use App\Traits\Email;
use App\Classes\Seller;
use App\Classes\LinkManager;

class ForgotPassword extends Form implements InputData{
	private $sellerModel;
	private $validation;
	private $linkManager;

	private $error;

	private $sellerId;

	use email;

	public function __construct(){
		$this->sellerModel = new SellerModel();
		$this->validation = new Validation();
		$this->linkManager =  new LinkManager();
		$this->seller = new Seller();
	}

	public function captureData(){
		$this->email = $_POST['email'];
	}

	public function validateData(){
		$this->error = "";

		try{
			/*
			* Set and validate email
			*/
			$this->setEmail($this->email);
			$this->email = $this->validEmail();

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return $this->error;
	}

	public function utilizeData(){		
		if($this->sendForgottenPasswordEmail()){
			return true;
		}else{
			false;
		}		
	}

	public function sendForgottenPasswordEmail(){
		$this->seller->setEmail($this->email);
		$this->sellerModel->setData($this->seller);
		$sellerData = $this->sellerModel->findSellerByEmail();
		$this->sellerId = $this->linkManager->encodeUrlId($sellerData['seller_id']);

		$this->message = "<div>				
				<h3 style='color:#000000;text-align:left'>Have you forgotten your password</h3>
				<p> 
					Click the button below to change your password.
				</p> 
				<p>
					<div style='border:#235cce solid 0.1em;background:#235cce;text-align:center;padding:1%;width:50%;margin-left:25%;border-radius:5px'>
						<a href='www.bukswap.com/changeforgottenpassword.php?id=".$this->sellerId."' style='font-size:14px;color:#ffffff;text-decoration:none'>
						Change password
						</a>
					</div>
				</p>
				<p>Thank you in advance,</p> 
				<p>bukswap team.</p>  
			</div>";
		$this->subject = "Bukswap: Forgotten password update";
		$this->body = $this->message;

		if($this->sendEmail()){
			return true;
		}
	}

	public function clearData(){}
}