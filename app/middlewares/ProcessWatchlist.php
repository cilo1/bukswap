<?php
namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\InputValidator;
use App\Classes\LinkManager;
use App\Classes\Watchlist;
use App\Models\WatchlistModel;

class ProcessWatchlist implements SessionMethods,InputData{

	private $inputValidator;
	private $linkManager;
	private $watchlist;
	private $watchlistModel;

	private $bookTitle;

	use Session;

	public function __construct(){
		$this->inputValidator = new InputValidator();
		$this->linkManager = new LinkManager();
		$this->watchlist = new Watchlist();
		$this->watchlistModel = new WatchlistModel();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		return $this->authenticateSessionData();
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}
	}

	public function captureData(){
		if(isset($_POST['title'])){
			$this->bookTitle = $this->inputValidator->validateInput($_POST['title']); 
		}

		if(isset($_GET['id'])){			
			$this->bookTitle = \base64_decode($_GET['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookTitle)){
			return true;
		}
	}

	public function utilizeData(){
		$this->watchlist->setBookTitle($this->bookTitle);
		$this->watchlist->setSellerId($_SESSION['seller_id']);

		$this->watchlistModel->setData($this->watchlist);

		if($_POST['action'] == "add" || $_GET['n'] == "watchlist"){		
			
			if($this->watchlistModel->createWatchlist() == true){
				return true;
			}else{			
				return false;
			}
		}

		if($_POST['action'] == "remove"){			
			if($this->watchlistModel->deleteWatchlistByTitle() == true){
				return true;
			}else{			
				return false;
			}
		}
	}

	public function clearData(){}
}