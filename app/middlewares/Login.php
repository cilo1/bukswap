<?php

namespace App\Middlewares;

use App\Classes\Form;
use App\Interfaces\InputData;
use App\Traits\Email;
use App\Traits\Password;
use App\Classes\Validation;
use App\Models\SellerModel;
use App\Traits\Session;
use App\Classes\Seller;

class Login extends Form implements InputData{

	public $email;
	public $pwd;
	public $sellerModel;
	public $seller;

	private $error;

	use Email;
	use Password;
	use Session;

	public function __construct(){
		$this->sellerModel = new SellerModel();
		$this->seller = new Seller();
	}

	public function captureData(){
		$this->email = $_POST['email'];
		$this->pwd = $_POST['password'];
	}

	public function validateData(){
		$this->error = "";

		try{
			/*
			* Set and validate email
			*/
			$this->setEmail($this->email);
			$this->email = $this->validEmail();

			/*
			* Set password
			*/
			$this->setPassword($this->pwd);

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return $this->error;
	}

	public function utilizeData(){
		$this->seller->setEmail($this->email);
		$this->sellerModel->setData($this->seller);
		$sellerInfo = $this->sellerModel->getSellerInfoByEmailAndStatus();			

		if($sellerInfo == false){							
			return "Email or password is incorrect!";				
		}else{

			$this->seller->setSellerId($sellerInfo['seller_id']);
			$this->seller->setFname($sellerInfo['fname']);
			$this->seller->setLname($sellerInfo['lname']);
			$this->seller->setEmail($sellerInfo['email']);

			if(\password_verify($this->pwd, $sellerInfo['password'])){					
				$this->startSession();
				$this->initializeSessionData($this->seller);

				if($this->authenticateSessionData() == true){
					return true;
				}
			}else{
				return "Password is incorrect!";
			}		
		}
	}

	public function clearData(){}
}