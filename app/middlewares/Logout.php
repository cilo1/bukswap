<?php

namespace App\Middlewares;

use App\Interfaces\SessionMethods;
use App\Interfaces\SessionLogout;
use App\Traits\Session;

class Logout implements SessionMethods, SessionLogout{

	use Session;

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		$this->authenticateSessionData();
	}

	public function utilizeSession(){
		if($this->killSession() == true){
			header("Location: index.php");
		}
	}

	public function killSession(){
		return $this->destroySession();
	}
}