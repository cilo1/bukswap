<?php

namespace App\Middlewares;

use App\Classes\Form;
use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\Validation;
use App\Classes\Book;
use App\Models\BookModel;

class SearchBook extends Form implements inputData,SessionMethods{

	private $searchItem;
	private $validation;
	private $book;

	use Session;

	public function __construct(){
		$this->validation = new Validation();
		$this->book = new Book();
		$this->bookModel = new BookModel();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false && !strstr($_SERVER['PHP_SELF'], "index.php")){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();
		$valerror = $this->validateData();

		if(empty($valerror)){
			return array("success",$this->utilizeData());
		}else{
			return array("error","<div class='error'>".$valerror."</div>");
		}		
	}

	public function captureData(){
		$this->searchItem = $_POST['item'];
	}

	public function validateData(){
		try{
			$this->searchItem = $this->validation->validStringField($this->searchItem, "Search item");
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function utilizeData(){
		$this->book->setTitle($this->searchItem);
		$this->bookModel->setData($this->book);
		$result = $this->bookModel->findBook();

		if($result != false){
			return $result;
		}else{
			return false;
		}
	}

	public function clearData(){}
}