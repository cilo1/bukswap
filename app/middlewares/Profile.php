<?php

namespace App\Middlewares;

use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Models\SellerModel;
use App\Classes\Seller;
use App\Classes\Form;
use App\Interfaces\InputData;
use App\Traits\Email;
use App\Traits\Phone;
use App\Traits\Password;
use App\Classes\Validation;

class Profile extends Form implements InputData,SessionMethods{

	public $sellerModel;
	public $seller;
	public $validation;

	private $fname;
	private $lname;
	private $location;
	private $country;
	private $emailInput;
	private $phone;
	private $currentPwd;
	private $newPwd;
	private $confirmNewPwd;

	use Session;
	use Email;
	use Phone;
	use Password;

	public function __construct(){
		$this->sellerModel = new SellerModel();
		$this->seller = new Seller();
		$this->validation = new Validation();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->seller->setSellerId($_SESSION['seller_id']);
		$this->sellerModel->setData($this->seller);
		$sellerInfo = $this->sellerModel->findSellerById();
		return $sellerInfo;
	}

	public function captureData(){

		switch($_POST['submit']){				
			case "Change bio":
				$this->fname = $_POST['fname'];
				$this->lname = $_POST['lname'];
				$this->location = $_POST['location'];
				$this->country = $_POST['country'];
			break;
			case "Change contacts":
				$this->emailInput = $_POST['email'];
				$this->phone = $_POST['phone'];
			break;
			case "Change password":
				$this->currentPwd = $_POST['currentPassword'];
				$this->newPwd = $_POST['newPassword'];
				$this->confirmNewPwd = $_POST['confirmNewPassword'];
			break;
		}		
	}

	public function validateData(){
		$this->error = "";

		try{
			switch($_POST['submit']){				
				case "Change bio":
					$this->fname = $this->validation->validStringName($this->fname, "First name");
					$this->lname = $this->validation->validStringName($this->lname, "Last name");
					$this->location = $this->validation->validStringName($this->location, "Location");
					$this->country = $this->validation->validSelectedOption($this->country, "Country");
				break;

				case "Change contacts":
					/*
					* Set and validate email
					*/
					$this->setEmail($this->emailInput);
					$this->emailInput = $this->validEmail();
					
					/*
					*validate phone number
					*/
					$this->setPhoneNumber($this->phone);
					$this->phone = $this->validPhoneNumber();
				break;

				case "Change password":
					$this->seller->setSellerId($_SESSION['seller_id']);
					$this->sellerModel->setData($this->seller);
					$sellerInfo = $this->sellerModel->findSellerById();

					if($sellerInfo == false){
						header("Location: ../views/index.php");
					}else{
						/*
						* Set current password
						*/
						if(password_verify($this->currentPwd, $sellerInfo['password'])){

							/*
							* validate and encrypt new password
							*/
							$this->setPassword($this->newPwd);
							$this->newPwd = $this->validPassword();
							$this->newPwd = $this->validConfirmPassword($this->confirmNewPwd);
							$this->newPwd = $this->encryptPassword();
						}else{
							$this->error = "Your current password is incorrect!";
						}
					}						
				break;
			}
		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return array("error",$this->error);	
	}

	public function utilizeData(){
		switch($_POST['submit']){				
			case "Change bio":
				if($this->updateBio() == true){
					return array("success","Your bio data was successfully updated!");	
				}else{
					return array("error","Your bio data was not updated. Please try again!");
				}
			break;	

			case "Change contacts":
				if($this->updateContacts() == true){
					return array("success","Your contacts data was successfully updated!");	
				}else{
					return array("error","Your contacts data was not updated. Please try again!");
				}
			break;	

			case "Change password":
				if($this->updatePassword() == true){
					return array("success","Your password was successfully updated!");	
				}else{
					return array("error","Your password was not updated. Please try again!");
				}
			break;	
		}
		
	}

	public function clearData(){}

	public function updateBio(){
		$this->seller->setFname($this->fname);
		$this->seller->setLname($this->lname);
		$this->seller->setLocation($this->location);
		$this->seller->setCountry($this->country);

		$this->sellerModel->setData($this->seller);
		
		if($this->sellerModel->updateSellerBio() == true){
			return true;
		}else{
			return false;
		}
	}

	public function updateContacts(){

		$this->seller->setEmail($this->emailInput);
		$this->seller->setPhone($this->phone);

		$this->sellerModel->setData($this->seller);
		
		if($this->sellerModel->updateSellerContacts() == true){
			return true;
		}else{
			return false;
		}
	}

	public function updatePassword(){

		$this->seller->setPassword($this->newPwd);

		$this->sellerModel->setData($this->seller);
		
		if($this->sellerModel->updateSellerPassword() == true){
			return true;
		}else{
			return false;
		}
	}
}