<?php
namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\Validation;
use App\Classes\LinkManager;
use App\Classes\Booklist;
use App\Models\BooklistModel;

class ProcessBooklist implements SessionMethods,InputData{
	private $validation;
	private $linkManager;
	private $booklist;
	private $booklistModel;

	private $bookId;

	use Session;

	public function __construct(){
		$this->validation = new Validation();
		$this->linkManager = new LinkManager();
		$this->booklist = new Booklist();
		$this->booklistModel = new BooklistModel();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		return $this->authenticateSessionData();
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}		
	}

	public function captureData(){
		if(isset($_POST['item'])){			
			$this->bookId = $this->linkManager->decodeUrlId($_POST['item']);
		}

		if(isset($_GET['id'])){
			$this->bookId = $this->linkManager->decodeUrlId($_GET['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}
	}

	public function utilizeData(){
		$this->booklist->setBookId($this->bookId);
		$this->booklist->setSellerId($_SESSION['seller_id']);

		$this->booklistModel->setData($this->booklist);

		if($_POST['action'] == "add" || $_GET['n'] == "booklist"){	

			if($this->booklistModel->createBooklist() == true){
				return true;
			}else{			
				return false;
			}
		}
		
		if($_POST['action'] == "remove"){		
			
			if($this->booklistModel->deleteBookFromBooklistById() == true){
				return true;
			}else{			
				return false;
			}
		}	
	}

	public function clearData(){}
}
