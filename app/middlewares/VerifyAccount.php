<?php

namespace App\Middleware;

use App\Classes\Validation;
use App\Models\SellerModel;
use App\Classes\Seller;
use App\Interfaces\InputData;

class VerifyAccount implements InputData{
	public $validation;

	public $email;
	public $verificationCode;	
	public $id;

	public function __construct(){
		$this->validation = new Validation();
		$this->sellerModel = new SellerModel();
		$this->seller = new Seller();
	}

	public function captureData(){
		/*
		* Capture data from verification link
		*/
		$this->id = $_GET['id'];
		$this->verificationCode = $_GET['code'];
	}

	public function validateData(){
		/*
		* Validate data
		*/
		$this->id = $this->validation->validateInput($this->id);
		$this->verificationCode = $this->validation->validateInput($this->verificationCode);
	}

	public function utilizeData(){
		$this->email = base64_decode($this->id);
		$this->seller->setEmail($this->email);
		$this->seller->setRegCode($this->verificationCode);
		$this->sellerModel->setData($this->seller);

		if($this->sellerModel->verifyAccount() == true){
			header("Location: profile.php");
		}
	}

	public function clearData(){}
}