<?php
namespace App\Middlewares;

use App\Interfaces\InputData;
use App\Classes\Form;
use App\Traits\Email;
use App\Traits\Phone;
use App\Traits\Password;
use App\Classes\Validation;
use App\Models\SellerModel;
use App\Classes\Seller;

class Registration extends Form implements InputData{ 
	public $validation;
	public $sellerModel;
	public $seller;

	public $token;
	public $fname;
	public $lname;
	public $email;
	public $phone;
	public $pwd;
	public $pwd2;
	public $location;
	public $country;
	public $code;

	private $error;

	use Email;
	use Phone;
	use Password;

	public function __construct(){
		//$this->token = $_SESSION['token'];
		$this->validation = new Validation;
		$this->sellerModel = new SellerModel();
		$this->seller = new Seller();
	}

	public function captureData(){		
		
		$this->fname = $_POST['fname'];
		$this->lname = $_POST['lname'];
		$this->email = $_POST['email'];
		$this->phone = $_POST['phone'];
		$this->pwd = $_POST['password'];
		$this->pwd2 = $_POST['password2'];
		$this->location = $_POST['location'];
		$this->country = $_POST['country'];
	}

	public function validateData(){

		try{
			/*
			* validate names
			*/
			$this->fname = $this->validation->validStringName($this->fname,"Firstname");
			$this->lname = $this->validation->validStringName($this->lname,"Lastname");

			/*
			* validate email
			*/
			$this->setEmail($this->email);
			$this->email = $this->validEmail();
			$this->seller->setEmail($this->email);
			$this->sellerModel->setData($this->seller);
			if($this->sellerModel->checkIfEmailExists() == true){
				throw new \Exception("Email already exists!");
			}

			/*
			* validate phone number
			*/
			$this->setPhoneNumber($this->phone);
			$this->phone = $this->validPhoneNumber();

			/*
			* validate and encrypt password
			*/
			$this->setPassword($this->pwd);
			$this->pwd = $this->validPassword();
			$this->pwd = $this->validConfirmPassword($this->pwd2);
			$this->pwd = $this->encryptPassword();

			/*
			* validate location
			*/
			$this->location = $this->validation->validStringLocation($this->location);

			/*
			* validate country
			*/
			$this->country = $this->validation->validSelectedOption($this->country, "Country");

			/*
			* validate email subject and add messaging - Account verification
			*/
			$subject = "Bukswap.com : Account verification info";
			$this->setSubject($subject);
			$this->validSubject($subject);

			/*
			* Verification code - Account verification
			*/
			$this->code = md5(uniqid(rand()));
			$key = base64_encode($this->email);
			$id = $key;

			/*
			* validate email body - Account verification
			*/
			$body = "<h2>Welcome to bukswap.com</h2>
					<p>
						<p>Hi ".$this->fname.",</p>
						<p>
							You're almost there! Please click the link below to activate your account.
						</p>
						<p>
						<div style='background:#4e77b7; text-align:center; width:150px; border-radius:5px; padding:5px; font-size:16px; text-decoration:none'><a style='color:#ffffff;' href='www.bukswap.com/app/views/verify.php?id=".$id."&&code=".$this->code."'>Activate account</a></div>
						</p>
					</p>
					<p>
						Thanks,<br/>
						The bukswap Team.
					</p>";
			$this->setBody($body);
			$this->validBody($body);

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}

		return $this->error;		
	}

	public function utilizeData(){

		// set data to seller
		$this->seller->setFname($this->fname);
		$this->seller->setLname($this->lname);
		$this->seller->setEmail($this->email);
		$this->seller->setPassword($this->pwd);
		$this->seller->setPhone($this->phone);
		$this->seller->setLocation($this->location);
		$this->seller->setCountry($this->country);
		$this->seller->setRegCode($this->code);

		/*
		* add seller details to db
		*/
		$this->sellerModel->setData($this->seller);

		if(empty($this->error)){		

			if($this->sellerModel->createSeller() == true){

				/*
				* Send email - Account verification
				*/
				if($this->sendEmail() == true){
					//return "Check your email to verify your account and complete registration!";
					header("Location: account_success.php");
				}
			}
		}		
	}

	public function clearData(){}
}