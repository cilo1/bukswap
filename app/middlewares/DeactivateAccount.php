<?php
namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Models\SellerModel;
use App\Classes\Seller;
use App\Models\BookModel;
use App\Classes\Book;
use App\Traits\Image;
use App\Traits\Password;

class DeactivateAccount implements SessionMethods,InputData{
	private $sellerModel;
	private $bookModel;
	private $book;

	private $pwd;

	use Session;
	use Image;
	use Password;

	public function __construct(){
		$this->seller = new Seller();
		$this->sellerModel = new SellerModel();
		$this->bookModel = new BookModel();
		$this->book = new Book();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: ../views/index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();

		if($this->validateData() == true){
			$this->utilizeData();
		}else{
			return false;
		}				
	}

	public function captureData(){
		if(isset($_POST['pwd'])){
			$this->pwd = $_POST['pwd'];
		}
	}

	public function validateData(){
		$this->seller->setSellerId($_SESSION['seller_id']);	
		$this->sellerModel->setData($this->seller);

		$sellerInfo = $this->sellerModel->findSellerById();

		if(password_verify($this->pwd, $sellerInfo['password'])){
			return true;
		}else{
			return 0;
		}
	}

	public function utilizeData(){
		$this->seller->setSellerId($_SESSION['seller_id']);	
		$this->sellerModel->setData($this->seller);		

		if($this->sellerModel->deleteSeller() == true){
			$this->book->setSellerId($_SESSION['seller_id']);
			$this->bookModel->setData($this->book);

			$rows = $this->bookModel->findMyBooks();
			$count = 0;
			$rowsTotal = count($rows);

			if($rowsTotal > 0){

				foreach($rows as $row){

					$this->setImageTargetDirectoryAndFile($row['book_img']);

					if($this->deleteImage() == true){

						$this->book->setBookId($row['book_id']);
						$this->bookModel->setData($this->book);

						if($this->bookModel->deleteBookById() == true){
							$count++;
						}
					} 
				}
				// if(count($rows) == $count){
				// 	$this->destroySession();
				// 	return true;
				// }else{
				// 	return false;
				// }
			}
			$this->destroySession();			
		}
	}

	public function clearData(){}

}