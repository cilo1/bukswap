<?php

namespace App\Middlewares;

use App\Interfaces\SessionMethods;
use App\Traits\Session;

class Home implements SessionMethods{

	use Session;

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}		
	}

	public function utilizeSession(){
		
	}

} 