<?php

namespace App\Middleware;

use App\Interfaces\InputData;
use App\Traits\Password;
use App\Classes\Seller;
use App\Models\SellerModel;
use App\Classes\LinkManager;

class ChangeForgottenPassword implements InputData{
	private $seller;
	private $sellerModel;

	private $linkManager;	

	private $sellerId;

	private $error;

	use Password;

	public function __construct(){
		$this->seller = new Seller();
		$this->sellerModel = new SellerModel();
		$this->linkManager = new LinkManager();
	}

	public function captureData(){
		$this->sellerId = $_POST['id'];
		$this->pwd = $_POST['password'];
		$this->pwd2 = $_POST['password2'];
	}

	public function validateData(){
		try{
			/*
			* validate and encrypt password
			*/
			$this->setPassword($this->pwd);
			$this->pwd = $this->validPassword();
			$this->pwd = $this->validConfirmPassword($this->pwd2);
			$this->pwd = $this->encryptPassword();

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return $this->error;
	}

	public function utilizeData(){
		if($this->changeForgottenPassword()){
			return true;
		}else{
			return false;
		}
	}

	public function changeForgottenPassword(){
		$this->seller->setSellerId($this->sellerId);
		$this->seller->setPassword($this->pwd);

		$this->sellerModel->setData($this->seller);
		
		if($this->sellerModel->updateSellerPassword() == true){
			return true;
		}else{
			return false;
		}
	}

	public function getSellerIdFromUrl(){
		return $this->linkManager->decodeUrlId($_GET['id']);
	}

	public function clearData(){}
}