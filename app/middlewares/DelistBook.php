<?php
namespace App\Middleware;

use App\Interfaces\SessionMethods;
use App\Interfaces\InputData;
use App\Traits\Session;
use App\Classes\LinkManager;
use App\Classes\Book;
use App\Models\BookModel;

class DelistBook implements SessionMethods, InputData{
	private $book;
	private $bookModel;
	private $linkManager;

	private $bookId;

	use Session;

	public function __construct(){
		$this->book = new Book();
		$this->bookModel = new BookModel();
		$this->linkManager = new LinkManager();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}		
	}

	public function captureData(){
		if(isset($_GET['id'])){
			$this->bookId = $this->linkManager->decodeUrlId($_GET['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}
	}

	public function utilizeData(){
		return $this->delistBook();
	}

	public function delistBook(){
		$this->book->setBookId($this->bookId);
		$this->bookModel->setData($this->book);

		if($this->bookModel->deleteBookById() == true){			
			return true;
		}else{
			return false;
		}
	}

	public function clearData(){}
} 