<?php

namespace App\Middleware;

use App\Classes\Book;
use App\Models\BookModel;
use App\Traits\Session;
use App\Interfaces\SessionMethods;
use App\Models\BooklistModel;
use App\Classes\Booklist;
use App\Models\WatchlistModel;
use App\Classes\Watchlist;
use App\Interfaces\InputData;

class MyBooks implements SessionMethods,InputData{
	private $book;
	private $bookModel;

	private $booklistModel;
	private $booklist;

	private $watchlistModel;
	private $watchlist;

	private $rows;

	use Session;

	public function __construct(){
		$this->book = new Book();
		$this->bookModel = new BookModel();

		$this->booklistModel = new BooklistModel();
		$this->booklist = new Booklist();

		$this->watchlistModel = new WatchlistModel();
		$this->watchlist = new Watchlist();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}		
	}

	public function captureData(){
		if(isset($_POST['id'])){			
			$this->bookId = $this->linkManager->decodeUrlId($_POST['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}		
	}

	public function utilizeData(){
		if($_POST['action'] == "delete_mybooks"){	
			$this->book->setBookId($this->bookId);
			$this->bookModel->setData($this->book);

			if($this->bookModel->deleteBookById() == true){
				return true;
			}else{
				return false;
			}
		}
	}

	public function clearData(){}

	public function myBooks(){
		$this->book->setSellerId($_SESSION['seller_id']);
		$this->bookModel->setData($this->book);
		$rows = $this->bookModel->findMyBooks();
		return array("mybooks",$rows);
	}

	public function myWatchlist(){
		$this->watchlist->setSellerId($_SESSION['seller_id']);

		$this->watchlistModel->setData($this->watchlist);
		$rows = $this->watchlistModel->findMyWatchlist();
		return array("mywatchlist",$rows);
	}

	public function myBooklist(){
		$this->booklist->setSellerId($_SESSION['seller_id']);

		$this->booklistModel->setData($this->booklist);
		$rows = $this->booklistModel->findMyBooklist();
		return array("mybooklist",$rows);
	}
}