<?php

namespace App\Middleware;

use App\Classes\Form;
use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session; 
use App\Models\BookModel;
use App\Classes\Book;
use App\Traits\Image;
use App\Classes\Validation;

class EditBook extends Form implements InputData,SessionMethods{
	public $bookId;

	public $imageUrl;
	public $title;
	public $publisher;
	public $country;
	public $eduLevel;
	public $eduClass;
	public $subject;
	public $price;
	public $condition;
	public $swap;

	private $error;

	public $validation;
	public $book;
	private $bookModel;	

	use Session;
	use Image;

	public function __construct(){
		$this->book = new Book();
		$this->bookModel = new BookModel();
		$this->validation = new Validation();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->book->setBookId($this->bookId);
		$this->bookModel->setData($this->book);
		return $this->bookModel->findBookById();
	}

	public function captureData(){

		if(!empty($_FILES["fileToUpload"]["tmp_name"])){
			$randImageNum = rand();
			$this->imageUrl = "book_img/".$randImageNum.".png";
			$this->setImageFile($_FILES["fileToUpload"]["tmp_name"]);
			$this->setImageTargetDirectoryAndFile($this->imageUrl);
		}

		$this->title = $_POST['title'];
		$this->publisher = $_POST['publisher'];
		$this->country = $_POST['country'];
		$this->eduLevel = $_POST['education-level'];
		$this->eduClass = $_POST['class'];
		$this->subject = $_POST['subject'];
		$this->price = $_POST['price'];
		$this->condition = $_POST['condition'];	
		if(isset($_POST['swap'])){
			$this->swap = 'y';
		}else{
			$this->swap = 'n';
		}
	}

	public function validateData(){
		$this->error = ""; 
		try{
			if(!empty($this->imageFile)){
				$this->validImageFile();
			}

			$this->title = $this->validation->validStringField($this->title,"Book title");
			$this->publisher = $this->validation->validStringField($this->publisher,"Book publisher");
			$this->country = $this->validation->validSelectedOption($this->country,"Country");
			$this->eduLevel = $this->validation->validSelectedOption($this->eduLevel,"Level");
			$this->eduClass = $this->validation->validSelectedOption($this->eduClass,"Class");
			$this->subject = $this->validation->validStringField($this->subject,"Subject");
			$this->price = $this->validation->validPrice($this->price);
			$this->condition = $this->validation->validateInput($this->condition);

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return array("error","<div class='error'>".$this->error."</div>");
	}

	public function utilizeData(){
		if(!empty($this->imageFile)){
			if($this->uploadImage() == true){
				$this->compressImage();	
			}
			$this->book->setImage($this->imageUrl);
		}			
			
		$this->book->setTitle($this->title);
		$this->book->setSellerId($_SESSION['seller_id']);
		$this->book->setPublisher($this->publisher);
		$this->book->setCountry($this->country);
		$this->book->setEduLevel($this->eduLevel);
		$this->book->setEduClass($this->eduClass);
		$this->book->setSubject($this->subject);
		$this->book->setPrice($this->price);
		$this->book->setCondition($this->condition);
		$this->book->setSwap($this->swap);

		$this->bookModel->setData($this->book);		
				
		if($this->bookModel->updateBook() == true){
			return array("success","<div class='success'>
						<h3><span>".$this->title."</span>, was successfully updated!</h3>
					</div>");
		}	
	}

	public function clearData(){}
}