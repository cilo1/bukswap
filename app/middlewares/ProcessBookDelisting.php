<?php

namespace App\Middleware;

use App\Interfaces\InputData;
use App\Interfaces\SessionMethods;
use App\Traits\Session;
use App\Classes\Validation;
use App\Classes\LinkManager;
use App\Classes\DelistedBook;
use App\Models\DelistedBookModel;
use App\Classes\Notification;
use App\Models\NotificationModel;
use App\Classes\Book;
use App\Models\BookModel;
use App\Traits\Email;
use App\Models\SellerModel;
use App\Classes\Seller;

class ProcessBookDelisting implements SessionMethods,InputData{
	private $validation;
	private $linkManager;
	private $delistedBook;
	private $delistedBookModel;
	private $notification;
	private $notificationModel;
	private $book;
	private $bookModel;
	private $sellerModel;

	private $bookId;
	private $tally;
	private $bookSellerId;
	private $bookTitle;

	private $message;	
	private $row;

	use Session;
	use Email;

	public function __construct(){
		$this->validation = new Validation();
		$this->linkManager = new LinkManager();
		$this->delistedBook = new DelistedBook();
		$this->delistedBookModel = new DelistedBookModel();
		$this->notification = new Notification();
		$this->notificationModel =  new NotificationModel();
		$this->book =  new Book();
		$this->bookModel = new BookModel();
		$this->seller = new Seller();
		$this->sellerModel = new SellerModel();
	}	 

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){
		$this->captureData();
		if($this->validateData() == true){			
			return $this->utilizeData();
		}else{
			return false;
		}		
	}

	public function captureData(){
		if(isset($_POST['id'])){			
			$this->bookId = $this->linkManager->decodeUrlId($_POST['id']);
		}
	}

	public function validateData(){
		if(!empty($this->bookId)){
			return true;
		}
	}

	public function utilizeData(){
		$this->delistedBook->setBookId($this->bookId);
		$this->delistedBookModel->setData($this->delistedBook);

		$this->book->setBookId($this->bookId);
		$this->bookModel->setData($this->book);
		$this->row = $this->bookModel->findBookById();
		$this->bookSellerId = $this->row['seller_id'];
		$this->bookTitle = $this->row['book_title'];
		$this->bookId = $this->linkManager->encodeUrlId($this->bookId);

		$this->message = "<div style='font-size:15px'>
				<p>
					A bukswap user has contacted you about this book:
				</p> 
				<h3 style='color:#000000;text-align:left'>".$this->bookTitle.".</h3>
				<p> 
					If you no longer have a copy of the book, kindly delist it from bukswap below.
				</p> 
				<p>
					<div style='border:#235cce solid 0.1em;background:#235cce;text-align:center;padding:1%;width:25%;margin-left:5%;border-radius:5px'>
						<a href='http://www.bukswap.com/delistbook.php?id=".$this->bookId."' style='font-size:14px;color:#ffffff;text-decoration:none'>
						Delist this book
						</a>
					</div>
				</p>
				<p>Thank you in advance,</p> 
				<p>bukswap team.</p>  
			</div>";

		$this->seller->setSellerId($this->bookSellerId);
		$this->sellerModel->setData($this->seller);
		$this->row = $this->sellerModel->findSellerById();
		$this->email = $this->row['email'];
		$this->subject = "Bukswap: Delist book if out of stock";
		$this->body = $this->message;	

		if($_POST['response'] == "No"){
			if($this->delistedBookModel->checkIfBookAlreadyExists() == true){

				$this->tally = $this->delistedBookModel->getDelistedBookTally();

				if($this->tally != false){
					$this->tally = $this->tally+1;

					$this->delistedBook->setBookId($this->bookId);
					$this->delistedBook->setTally($this->tally);

					$this->delistedBookModel->setData($this->delistedBook);

					if($this->delistedBookModel->updateDelistedBookTally() == true){
						$this->sendEmail();	
						return true;
					}else{			
						return false;
					}
				}				
			}else{
				if($this->delistedBookModel->createDelistedBook() == true){
					$this->notification->setNotificationRecipient($this->bookSellerId);
					$this->notification->setNotificationMessage($this->message);

					$this->notificationModel->setData($this->notification);

					if($this->notificationModel->createNotification() == true){
						$this->sendEmail();
						return true;
					}else{
						return false;
					}
				}else{			
					return false;
				}
			}			
		}
		
		if($_POST['response'] == "Yes"){
			
			//only send notification
			$this->notification->setNotificationRecipient($this->bookSellerId);
			$this->notification->setNotificationMessage($this->message);

			$this->notificationModel->setData($this->notification);

			if($this->notificationModel->createNotification() == true){

				$this->sendEmail();					
				return true;
			}else{
				return false;
			}
		}	
	}

	public function clearData(){}
}
