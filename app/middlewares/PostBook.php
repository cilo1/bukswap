<?php

namespace App\Middleware;

use App\Classes\Form;
use App\Interfaces\SessionMethods;
use App\Interfaces\InputData;
use App\Traits\Image;
use App\Traits\Session;
use App\Classes\Validation;
use App\Models\BookModel;
use App\Classes\Book;

class PostBook extends Form implements SessionMethods, InputData{	
	public $imageUrl;
	public $title;
	public $publisher;
	public $country;
	public $eduLevel;
	public $eduClass;
	public $subject;
	public $price;
	public $condition;
	public $swap;

	private $error;

	public $bookModel;
	public $validation;
	public $book;

	use Session;
	use Image;

	public function __construct(){
		$this->validation = new Validation();
		$this->bookModel = new BookModel();
		$this->book = new Book();
	}

	public function beginSession(){
		$this->startSession();
	}

	public function authenticateSession(){
		if($this->authenticateSessionData() == false){
			header("Location: index.php"); 
		}
	}

	public function utilizeSession(){		
		$this->captureData();
		$valerror = $this->validateData();
		
		if(!empty($valerror)){			
			$this->clearData();
			return array("error","<div class='error'>".$valerror."</div>");	
		}else{
			$dataExists = $this->utilizeData();
			if(empty($dataExists)){
				return array("error","<div class='error'>This book has already been posted by this account!</div>");
			}else{
				$this->clearData();
				return array("success",$dataExists);
			}
		}		
	}

	public function captureData(){
		$randImageNum = rand();
		$this->imageUrl = "book_img/".$randImageNum.".png";
		$this->setImageFile($_FILES["fileToUpload"]["tmp_name"]);
		$this->setImageTargetDirectoryAndFile($this->imageUrl);
		$this->title = $_POST['title'];
		$this->publisher = $_POST['publisher'];
		$this->country = $_POST['country'];
		$this->eduLevel = $_POST['education-level'];
		$this->eduClass = $_POST['class'];
		$this->subject = $_POST['subject'];
		$this->price = $_POST['price'];
		$this->condition = $_POST['condition'];	
		if(isset($_POST['swap'])){
			$this->swap = 'y';
		}else{
			$this->swap = 'n';
		}			
	}

	public function validateData(){
		$this->error = ""; 
		try{
			$this->validImageFile();

			$this->title = $this->validation->validStringField($this->title,"Book title");
			$this->publisher = $this->validation->validStringField($this->publisher,"Book publisher");
			$this->country = $this->validation->validSelectedOption($this->country,"Country");
			$this->eduLevel = $this->validation->validSelectedOption($this->eduLevel,"Level");
			$this->eduClass = $this->validation->validSelectedOption($this->eduClass,"Class");
			$this->subject = $this->validation->validStringField($this->subject,"Subject");
			$this->price = $this->validation->validPrice($this->price);
			$this->condition = $this->validation->validateInput($this->condition);

		}catch(\Exception $e){
			$this->error = $e->getMessage()."\n";
		}
		return $this->error;
	}

	public function utilizeData(){

		if($this->uploadImage() == true){
			$this->compressImage();			
			
			$this->book->setTitle($this->title);
			$this->book->setSellerId($_SESSION['seller_id']);
			$this->book->setImage($this->imageUrl);
			$this->book->setPublisher($this->publisher);
			$this->book->setCountry($this->country);
			$this->book->setEduLevel($this->eduLevel);
			$this->book->setEduClass($this->eduClass);
			$this->book->setSubject($this->subject);
			$this->book->setPrice($this->price);
			$this->book->setCondition($this->condition);
			$this->book->setSwap($this->swap);

			$this->bookModel->setData($this->book);

			if($this->bookModel->checkIfBookHasAlreadyBeenPosted() == false){			
				
				if($this->bookModel->createBook() == true){
					return "<div class='success'>
							<h3><span>".$this->title."</span>, was successfully uploaded!</h3>
							<p>
							  You can preview or edit the book, or post another book from the links below: 
							</p>
							<p>
								<div class='book-post-success-btn'>
									<a href=''>Preview posted book</a>
								</div>
								<div class='book-post-success-btn'>
									<a href='postbook.php'>Post another book</a>
								</div>
								<div class='clear'></div>
							</p>
							</div>";
				}
			}		
		}		
	}

	public function clearData(){
		$this->imageUrl = "";
		$this->title = "";
		$this->publisher = "";
		$this->country = "";
		$this->eduLevel = "";
		$this->eduClass = "";
		$this->subject = "";
		$this->price = "";
		$this->condition = "";	
		$this->swap = "";		
	}
}