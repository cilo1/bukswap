<?php

namespace App\Interfaces;

interface TemplateMethods{

	public function header();

	public function content();
	
	public function footer();
}