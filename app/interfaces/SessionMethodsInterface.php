<?php

namespace App\Interfaces;

interface SessionMethods{

	public function beginSession();

	public function authenticateSession();

	public function utilizeSession();

}