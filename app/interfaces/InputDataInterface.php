<?php

namespace App\Interfaces;

interface InputData{

	public function captureData();

	public function validateData();

	public function utilizeData();

	public function clearData();
}