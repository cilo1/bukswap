<?php

namespace App\Interfaces;

interface SessionLogout{

	public function killSession();
}