<?php
namespace App\Classes;

class Form{

	public function __construct(){
		//error_reporting(0);
	}

	public function generatedFormToken(){
		$token = md5(uniqid(rand(), TRUE));
		$_SESSION['token'] = $token;
		$_SESSION['token_time'] = time();

		return $_SESSION['token'] ;
	}

	public function validateFormInput($input) {
		$input = trim($input);
		$input = stripslashes($input);
		$input = htmlspecialchars($input);
		return $input;
	}

}