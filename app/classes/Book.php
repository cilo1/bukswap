<?php

namespace App\Classes;

class Book{

	public $bookId;
	public $sellerId;
	public $bookTitle;
	public $bookImg;
	public $publisher;
	public $eduLevel;
	public $country;
	public $eduClass;
	public $subject;
	public $price;
	public $condition;
	public $swap;

	public function setBookId($bookId){
		$this->bookId = $bookId; 
	}

	public function setSellerId($sellerId){
		$this->sellerId = $sellerId; 
	}

	public function setTitle($bookTitle){
		$this->bookTitle = $bookTitle; 
	}

	public function setImage($bookImg){
		$this->bookImg = $bookImg; 
	}

	public function setPublisher($publisher){
		$this->publisher = $publisher; 
	}

	public function setEduLevel($eduLevel){
		$this->eduLevel = $eduLevel; 
	}

	public function setCountry($country){
		$this->country = $country; 
	}

	public function setEduClass($eduClass){
		$this->eduClass = $eduClass; 
	}

	public function setSubject($subject){
		$this->subject = $subject; 
	}

	public function setPrice($price){
		$this->price = $price; 
	}

	public function setCondition($condition){
		$this->condition = $condition; 
	}

	public function setSwap($swap){
		$this->swap = $swap; 
	}

	public function getBookId(){
		return $this->bookId; 
	}

	public function getSellerId(){
		return $this->sellerId; 
	}

	public function getTitle(){
		return $this->bookTitle; 
	}

	public function getImage(){
		return $this->bookImg; 
	}

	public function getPublisher(){
		return $this->publisher; 
	}

	public function getEduLevel(){
		return $this->eduLevel; 
	}

	public function getCountry(){
		return $this->country; 
	}

	public function getEduClass(){
		return $this->eduClass; 
	}

	public function getSubject(){
		return $this->subject; 
	}

	public function getPrice(){
		return $this->price; 
	}

	public function getCondition(){
		return $this->condition; 
	}

	public function getSwap(){
		return $this->swap; 
	}
}