<?php
namespace App\Classes;

class Booklist{

	public $booklistId;
	public $bookId;
	public $sellerId;
	public $dateCreated;

	public function setBooklistId($booklistId){
		$this->booklistId = $booklistId;
	} 

	public function getBooklistId(){
		return $this->booklistId;
	}

	public function setBookId($bookId){
		$this->bookId = $bookId;
	} 

	public function getBookId(){
		return $this->bookId;
	}

	public function setSellerId($sellerId){
		$this->sellerId = $sellerId;
	} 

	public function getSellerId(){
		return $this->sellerId;
	}

	public function setDateCreated($dateCreated){
		$this->dateCreated = $dateCreated;
	} 

	public function getDateCreated(){
		return $this->dateCreated;
	}

}