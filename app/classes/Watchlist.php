<?php

namespace App\Classes;

class Watchlist{

	public $watchlistId;
	public $sellerId;
	public $bookTitle;

	public function setWatchlistId($watchlistId){
		$this->watchlistId = $watchlistId;
	}

	public function getWatchlistId(){
		return $this->watchlistId;
	}

	public function setSellerId($sellerId){
		$this->sellerId = $sellerId;
	}

	public function getSellerId(){
		return $this->sellerId;
	}

	public function setBookTitle($bookTitle){
		$this->bookTitle = $bookTitle;
	}

	public function getBookTitle(){
		return $this->bookTitle;
	} 

}