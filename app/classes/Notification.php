<?php
namespace App\Classes;

class Notification{

	private $notificationId;
	private $notificationRecipient;
	private $notificationMessage;
	private $dateCreated;

	public function setNotificationId($notificationId){
		$this->notificationId = $notificationId;
	}

	public function getNotificationId(){
		return $this->notificationId;
	}

	public function setNotificationRecipient($notificationRecipient){
		$this->notificationRecipient = $notificationRecipient;
	}

	public function getNotificationRecipient(){
		return $this->notificationRecipient;
	}

	public function setNotificationMessage($notificationMessage){
		$this->notificationMessage = $notificationMessage;
	}

	public function getNotificationMessage(){
		return $this->notificationMessage;
	}

	public function setDateCreated($dateCreated){
		$this->dateCreated = $dateCreated;
	} 

	public function getDateCreated(){
		return $this->dateCreated;
	}
}