<?php

namespace App\Classes;

class DelistedBook{

	public $delistedBookId;
	public $bookId;
	public $tally;
	public $dateCreated;

	public function setDelistedBookId($delistedBookId){
		$this->delistedBookId = $delistedBookId;
	}

	public function setBookId($bookId){
		$this->bookId = $bookId;
	} 

	public function setTally($tally){
		$this->tally = $tally;
	} 

	public function setDateCreated($dateCreated){
		$this->dateCreated = $dateCreated;
	} 

	public function getDelistedBookId(){
		return $this->delistedBookId;
	}

	public function getBookId(){
		return $this->bookId;
	}

	public function getTally(){
		return $this->tally;
	} 

	public function getDateCreated(){
		return $this->dateCreated;
	}
}