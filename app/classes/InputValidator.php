<?php
namespace App\Classes;

class InputValidator{

	public function __construct(){
		//error_reporting(0);
	}

	function validateInput($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
}