<?php
namespace App\Classes;

class ReportedPost{
	public $reportedPostId;
	public $bookId;
	public $claim;
	public $datecreated;

	public function setReportedPostId($reportedPostId){
		$this->reportedPostId = $reportedPostId;
	}

	public function setBookId($bookId){
		$this->bookId = $bookId;
	}

	public function setClaim($claim){
		$this->claim = $claim;
	}

	public function setDateCreated($datecreated){
		$this->datecreated = $datecreated;
	}

	public function getReportedPostId(){
		return $this->reportedPostId;
	} 

	public function getBookId(){
		return $this->bookId;
	}

	public function getClaim(){
		return $this->claim;
	}

	public function getDateCreated(){
		return $this->datecreated;
	}
}