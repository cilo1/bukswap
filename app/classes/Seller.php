<?php

namespace App\Classes;

class Seller{

	public $sellerId;
	public $fname;
	public $lname;
	public $email;
	public $phone;
	public $pwd;
	public $location;
	public $country;
	public $regcode;

	public function setSellerId($sellerId){
		$this->sellerId = $sellerId;
	}

	public function setFname($fname){
		$this->fname = $fname;
	}

	public function setLname($lname){
		$this->lname = $lname;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}

	public function setPassword($pwd){
		$this->pwd = $pwd;
	}

	public function setLocation($location){
		$this->location = $location;
	}

	public function setCountry($country){
		$this->country = $country;
	}

	public function setRegCode($regcode){
		$this->regcode = $regcode;
	} 

	public function getSellerId(){
		return $this->sellerId;
	}

	public function getFname(){
		return $this->fname;
	}

	public function getLname(){
		return $this->lname;
	}

	public function getEmail(){
		return $this->email;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function getPassword(){
		return $this->pwd;
	}

	public function getLocation(){
		return $this->location;
	}

	public function getCountry(){
		return $this->country;
	}

	public function getRegCode(){
		return $this->regcode;
	}
}