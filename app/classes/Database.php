<?php
namespace App\Classes;

class Database{

	public $server;
	public $user;
	public $pass;
	public $db; 
	public $mysqli;

	function __construct(){
		//Error_reporting(0);
		date_default_timezone_set("America/New_York");
	}

	function connectToMysqli(){
		$this->server = "127.0.0.1";
		$this->user  = "root";
		$this->pass = "";
		$this->db = "bukswap";

		$this->mysqli = new \mysqli($this->server,$this->user,$this->pass,$this->db);

		if($this->mysqli->connect_errno){
			throw new \Exception("Sorry we are having some error");
		}else{
			return $this->mysqli; 
		}
	}	
}