<?php

namespace App\Classes;

use App\Classes\InputValidator;

class Validation extends InputValidator{

	public function validStringName($name,$item){
		if(empty($name)){
			throw new \Exception($item." cannot be empty!");
		}

		if(preg_match('/\d/', $name) || preg_match('/[^a-zA-Z\d]/', $name)){
			throw new \Exception($item." cannot contain numbers and special characters!");
		}
		$name = $this->validateInput($name);

		return $name;
	}

	public function validStringField($field,$item){
		if(empty($field)){
			throw new \Exception($item." cannot be empty!");
		}
		$field = $this->validateInput($field);

		return $field;
	}

	public function validStringLocation($location){
		if(empty($location)){
			throw new \Exception("Location cannot be empty!");
		}

		if(preg_match('/[^a-zA-Z\d]/', $location)){
			throw new \Exception("Location cannot contain special characters!");
		}
		$location = $this->validateInput($location);

		return $location;
	}

	public function validSelectedOption($option,$item){
		if($option == "0" || $option == null || empty($option)){
			throw new \Exception($item. " must be selected!");
		}
		$option = $this->validateInput($option);

		return $option;
	}

	public function validPrice($price){
		if(empty($price)){
			throw new \Exception("Price cannot be empty!");
		}

		if(!is_numeric($price)){
			throw new \Exception("Price must be numeric values!");
		}
		$location = $this->validateInput($price);

		return $price;
	}
}